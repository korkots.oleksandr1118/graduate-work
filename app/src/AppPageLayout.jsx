import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { getUser } from "./api/userService";
import NavBar from "./components/NavBar";
import Toast from './components/Toast'
import { getCurrentUser } from './api/userService'


const defaultExecutionStatus = {
    AlgorithmName: "",
    IsTestExecution: null,
    CurrentStepNumber: 0,
    FailedStepAmount: 0,
    CurrentArray: [],
    RemainTime: null,
    StepsCount: null, 
}
  
const defaultExecutionResult = {
    Mark: null,
    ExecutionTime: null,
    AlgorithmTypeName: null,
    FailedStep: null,
    IsTestExecution: null,
}

const defaultUser = {
    FirstName: null,
    MiddleName: null,
    LastName: null,
    RoleId: null
}

const defaultToast = {
    type: null,
    text: null
}

const UserContext = React.createContext(defaultUser);

const PageLayout = ({title, body}) => {
    const [executionStatus, setExecutionStatus] = useState(defaultExecutionStatus);
    const [executionResult, setExecutionResult] = useState(defaultExecutionResult);
    const [showToast, setShowToast] = useState(defaultToast);
    const [algorithmTypes, setAlgorithmTypes] = useState([]);
    const [user, setUser] = useState(defaultUser)
    const location = useLocation()
  
    const managePage = {
        executionStatus: executionStatus,
        setExecutionStatus: setExecutionStatus,
        algorithmTypes: algorithmTypes,
        setAlgorithmTypes: setAlgorithmTypes,
        executionResult: executionResult,
        setExecutionResult: setExecutionResult,
        showToast: showToast,
        setShowToast: setShowToast,
        setUser: setUser
    }

    useEffect(() => {
        if (user?.RoleId === null && ["/login","/registration"].reduce((prev, curr) => prev && curr !== location.pathname, true)) {
            getCurrentUser(setUser)
        }
    }, [location])

    return (
        <UserContext.Provider value={user}>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-7">
                        {title(executionStatus)}
                        <NavBar setUser={setUser}/>
                        {body({managePage})}
                    </div>
                </div>
            </div> 
            <Toast toast={showToast.text} type={showToast.type} setToast={setShowToast}/> 
        </UserContext.Provider>
    )
}

export { PageLayout, UserContext }