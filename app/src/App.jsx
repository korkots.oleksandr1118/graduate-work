import RegistrationPage from './containers/RegistrationPage.jsx'
import React, { useDeferredValue, useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
} from "react-router-dom";
import UsersTablePage from './containers/UsersTablePage.jsx';
import ResultTablePage from './containers/ResultTablePage';
import LoginPage from './containers/LoginPage.jsx';
import SelectAlgorithmTypePage from './containers/SelectAlgorithmTypePage'
import { PageLayout } from './AppPageLayout'
import ExecutionPage from './containers/ExecutionPage'
import ResultPage from "./containers/ResultPage"
import NotFoundPage from './containers/NotFoundPage'
import UserFormPage from './containers/UserFormPage'
import ExecutionTitle from './components/ExecutionTitle'
import PageTitle from './components/PageTitle';

const defaultExecutionStatus = {
  UserFM: "",
  AlgorithmName: "",
  IsTestExecution: null,
  CurrentStepNumber: 0,
  FailedStepAmount: 0,
  CurrentArray: [],
  RemainTime: null,
  StepsCount: null, 
}

const defaultExecutionResult = {
  Mark: null,
  ExecutionTime: null,
  AlgorithmTypeName: null,
  FailedStep: null,
  IsTestExecution: null,
}

let routes = {
  users: { path: "/users", title: () => <PageTitle title="Registrated users" />, body: (props) => <UsersTablePage {...props} />},
  results: { path: "/results", title: () => <PageTitle title="Results"/>, body: (props) => <ResultTablePage {...props} />},
  createUser: { path: "/users/create", title: () => <PageTitle title="Create new user"/>, body: (props) => <UserFormPage {...props} />},
  editUser: { path: "/users/:userId/edit", title: () => <PageTitle title="Edit user"/>, body: (props) => <UserFormPage {...props} />},
  registration: { path: "/registration", title: () => <PageTitle title="Registration"/>, body: (props) => <RegistrationPage {...props} />},
  login: { path: "/login", title: () => <PageTitle title="Login"/>, body: (props) => <LoginPage {...props} />},
  execution: { path: "/execution", title: (status) => <ExecutionTitle status={status} />, body: (props) => <ExecutionPage {...props} />},
  result: { path: "/execution_result", title: () => <PageTitle title="Execution result"/>, body: (props) => <ResultPage {...props} />},
  selectAlgorithm: { path: "/", exact: true, title: () => <PageTitle title="Select sort algorism"/>, body: (props) => <SelectAlgorithmTypePage {...props} />}
}

const App = () => {
  return (
    <Router>
      <Routes>
        { Object.keys(routes).map((key) => 
          <Route key={key} 
                 path={routes[key].path}
                 exact={routes[key].exact}
                 element={<PageLayout title={routes[key].title} 
                                      body={routes[key].body}                                      
                            />} 
          />
        )}
        <Route path='*' element={<NotFoundPage />} />
      </Routes>
    </Router>
  );
}

export default App;
