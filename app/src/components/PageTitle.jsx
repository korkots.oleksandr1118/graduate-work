const PageTitle = ({title}) => {
    return (
        <div className="py-2 mt-4">
            <div className="text-center h2">{title}</div>
        </div> 
    ) 
}

export default PageTitle