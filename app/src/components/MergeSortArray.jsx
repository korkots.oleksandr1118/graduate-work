import ArrayElementValue from './ArrayElementValue'
import ArrayElementBox from './ArrayElementBox'
import { useState, useEffect } from 'react'

const MergeSortArray = ({currentArray, onChange, stepsCount, currentStepNumber, firstStepArray, nextExecutionStep}) => {
    const [arraySteps, setArraySteps] = useState([])

    useEffect(() => {getArraysForSteps()},[])

    const onDrop = (array, dragElement, dropElement) => {
        let newCurrentArray = array.map((row) => row.map((col) => col));   

        newCurrentArray[dragElement.index.row][dragElement.index.col][dragElement.index.item] = "_";
        newCurrentArray[dropElement.index.row][dropElement.index.col][dropElement.index.item] = dragElement.value;
        onChange(getArrayByStep(currentStepNumber + 1))
        setArraySteps(newCurrentArray)
    }

    const getArraysForSteps = () => {
        let newArray = []
        saveRow(firstStepArray, stepsCount - 1, newArray)
        setArraySteps(newArray);
    }

    const saveRow = (mas, step, ans) => {
        if (step === -1) return
        if (!ans[step]) ans[step] = []
        step === 0 ? ans[step].push(mas) : ans[step].push([...Array(mas.length).fill("_")]);
        saveRow(mas.slice(0, mas.length / 2), step - 1, ans)
        saveRow(mas.slice(mas.length / 2,  mas.length), step - 1, ans)
    }

    const getArrayByStep = (step) => {
        const row = arraySteps[step]
        return row?.reduce((prev, curr) => [...prev, ...curr], [])
    }

    const setCurrentArray = () => {
        if (currentArray.filter(val => val === "_").length === 0) {
            nextExecutionStep()
        }
    }

    return (
        <div>
            {arraySteps.map((value, indexI) => (
                <div className='row px-3 mb-4' key={`row_${indexI}`}>
                    <div className='col mx-2'>
                        <div className='row'>
                            {value.map((array, indexY) => (
                                <div className='col mx-2' key={`col_${indexY}`}>
                                    <div className='row'>
                                        {array.map((number, index) => (
                                            <ArrayElementBox key={`item_${index}`} onDrop={onDrop} array={arraySteps} element={{index: {row: indexI, col: indexY, item: index}, value: number}} stepNumber={currentStepNumber}> 
                                                <ArrayElementValue element={{index: {row: indexI, col: indexY, item: index}, value: number}} />
                                            </ArrayElementBox>))}
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            ))}
            <div className=" d-flex justify-content-end" >
                <button className="btn btn-primary" onClick={setCurrentArray}>Next step</button>
            </div>
        </div>
    )
}

export default MergeSortArray