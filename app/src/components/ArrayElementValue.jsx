import { useEffect } from 'react'
import { useDrag } from 'react-dnd'


const ArrayElementValue = ({element}) => {
  const [{isDragging}, drag] = useDrag(() => ({
      type: "element",
      item: element,
      collect: monitor => ({
        isDragging: !!monitor.isDragging(),
      }),
    }),[element])

  return (
      <div className="p-1 text-center"
            ref={drag}
            style={{
                opacity: isDragging ? 0.5 : 1,
                cursor: 'move',
            }}>
          {isDragging ? "_" : element.value}
      </div>
  )
}

export default ArrayElementValue