import { useEffect, useState } from "react"
import moment from "moment";

const Timer = ({remainTime, timeExpired}) => {
    const [time, setTime] = useState(moment(remainTime))

    useEffect(() => {
        if ((+time.seconds() <= 0 && +time.minutes() <= 0) || remainTime < 0) {
            timeExpired()
            return 
        }
        const intervalId = setInterval(() => {
            setTime(moment(time).subtract('second', 1))
        }, 1000)
        
        return () => clearInterval(intervalId);
    }, [time])

    return (
        <div className="mb-2">
            <span>Remaining time: </span>
            <span className="border border-info p-1">{time.format('mm:ss')}</span>
        </div>
    )
}

export default Timer