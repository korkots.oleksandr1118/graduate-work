import { useContext, useMemo } from "react"
import { useNavigate, useLocation } from "react-router-dom"
import { logoutUser } from '../api/userService'
import { UserContext } from '../AppPageLayout'


const roles = {
    student: 3,
    teacher: 2,
    admin: 1
}

const NavBar = ({setUser}) => {
    const navigate = useNavigate()
    const location = useLocation()
    const user = useContext(UserContext)

    const rightMenuItems =  useMemo(() => [
        {
            key: "logout", 
            name: "Logout", 
            condition: ["/login","/registration"].reduce((prev, curr) => prev && curr !== location.pathname, true),
            action: () => logoutUser(setUser).then(({navigate_to}) => navigate(navigate_to, {replace: true}))
        },
    ], [location, user])

    const leftMenuItems = useMemo(() => [
        {
            key: "users", 
            name: "Users", 
            condition: ["/login","/registration"].reduce((prev, curr) => prev && curr !== location.pathname, true) && user.RoleId !== roles.student && user.RoleId !== roles.teacher,
            action: () => navigate("/users", {replace: true}),
            location: ["/users"]
        },
        {
            key: "results", 
            name: "Results", 
            condition: ["/login","/registration"].reduce((prev, curr) => prev && curr !== location.pathname, true) && user.RoleId !== roles.student,
            action: () => navigate("/results", {replace: true}),
            location: ["/results"]
        },
        {
            key: "execution", 
            name: "Execution", 
            condition: ["/login","/registration"].reduce((prev, curr) => prev && curr !== location.pathname, true) && user.RoleId !== roles.student,
            action: () => navigate("/execution", {replace: true}),
            location: ["/execution", "/"],
        },
        {
            key: "login", 
            name: "Login", 
            condition: ["/registration"].reduce((prev, curr) => prev && curr === location.pathname, true),
            action: () => navigate("/login", {replace: true}),
            location: ["/login"],
        },
        {
            key: "registration", 
            name: "Registration", 
            condition: ["/login"].reduce((prev, curr) => prev && curr === location.pathname, true),
            action: () => navigate("/registration", {replace: true}),
            location: ["/registration"],
        },
        
    ], [location, user])

    const getTabStatus = (tabLocations) => (
        tabLocations.reduce((prev, curr) => prev || curr === location.pathname, false) ? "active bg-secondary text-white" : ""
    )

    return (
        <nav className="navbar navbar-expand navbar-light bg-light mb-2">
            <div className="container-fluid justify-content-between">
                <ul className="navbar-nav">
                    {leftMenuItems.map((item) => (
                        item.condition && <li className="nav-item" key={item.key}>
                            <button className={`nav-link btn ${getTabStatus(item.location)}`} onClick={item.action}>{item.name}</button>
                        </li>
                    ))}
                </ul>
                <ul className="nav navbar-nav">
                    {rightMenuItems.map((item) => (
                        item.condition && <li className="nav-item" key={item.key}>
                            <button className="btn btn-link" onClick={item.action}>{item.name}</button>
                        </li>
                    ))}
                </ul>
            </div>
        </nav>
    )
}

export default NavBar