import { useEffect } from "react"

const UserForm = (props) => {
    const {fields, values, onChange, onSave} = props  
    
    return (
        <div className="card">
            <div className="card-body">
                {
                    fields.map((field, index) => (
                        <div key={`field - ${index}`} className="mt-4">
                            <label className="form-label">{field.label}</label>
                            <input className="form-control"
                                   value={values?.[field.name]}
                                   type={field.type || 'text'}
                                   onChange={(event) => onChange({...values, [field.name]: event.target.value})}
                            />
                        </div>
                    ))
                }
                <div className="mt-4 ">
                    <button className="btn btn-primary"
                            onClick={onSave}
                    >
                        Save
                    </button>
                </div>
            </div>
        </div>
    )
}

export default UserForm

