import { useDrop } from "react-dnd";

const ArrayElementBox = ({array, onDrop, element, children, stepNumber = 0}) => {
  const [{ isOver }, drop] = useDrop(() => ({
      accept: "element",
      drop: (item) => onDrop(array, item, element),
      canDrop: () => element.value == "_",
      collect: monitor => ({
        isOver: !!monitor.isOver(),
      }),
    }), [array, stepNumber])

  return (
      <div ref={drop} className="col border border-black">
        {children}
      </div>
  )
}

export default ArrayElementBox