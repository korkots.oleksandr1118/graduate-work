const ExecutionTitle = ({status}) => {
    return (
        <div className="py-2 my-4">
            <div className="text-center h2">{status.UserFM}</div>
            <div className="text-center text-decoration-underline ">{status.IsTestExecution ? "test execution (result will not be saved)": "official execution"} </div>
        </div> 
    ) 
}

export default ExecutionTitle