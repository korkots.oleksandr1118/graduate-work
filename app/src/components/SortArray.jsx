import ArrayElementValue from './ArrayElementValue'
import ArrayElementBox from './ArrayElementBox'


const SortArray = ({currentArray, onChange, swapElement, setSwapElement, nextExecutionStep}) => {
    const onDrop = (array, dragElement, dropElement) => {
        let newCurrentArray = array.map((x) => x);   
        if (dragElement.index === -1) {
            newCurrentArray[dropElement.index] = dragElement.value;
            setSwapElement("_");
        } else {
            newCurrentArray[dragElement.index] = "_";
            newCurrentArray[dropElement.index] = dragElement.value;
        }
        
        onChange(newCurrentArray)
    }

    const onDropSwap = (array, dragElement) => {
        let newCurrentArray = array.map((x) => x);  
        newCurrentArray[dragElement.index] = "_";
        onChange(newCurrentArray)
        setSwapElement(dragElement.value)        
    }

    return (
        <div>
            <div className='row px-3 mb-4'>
                {currentArray.map((number, index) => (
                    <ArrayElementBox onDrop={onDrop} array={currentArray} element={{index: index, value: number}} key={`element-${index}`}> 
                        <ArrayElementValue element={{index: index, value: number}} />
                    </ArrayElementBox>
                ))}
            </div>
            <div>
                <span>Element for swap</span>
            </div>
            <div className='row row-cols-auto px-3 mb-4'>
                <ArrayElementBox onDrop={onDropSwap} array={currentArray} element={{index: -1, value: swapElement}}> 
                    <ArrayElementValue element={{index: -1, value: swapElement}}/>
                </ArrayElementBox>
            </div>
            <div className=" d-flex justify-content-end" >
                <button className="btn btn-primary" onClick={nextExecutionStep}>Next step</button>
            </div>
        </div>
    )
}

export default SortArray

