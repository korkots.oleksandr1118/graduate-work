import { useEffect } from 'react'


const toastTypes = {
    "success": "bg-success text-white",
    "warning": "bg-warning text-dark",
    "danger": "bg-danger text-white",
}

const Toast = ({toast, setToast, type}) => {
    useEffect(() => {
        if (toast !== null){
            const intervalId = setInterval(() => {
                setToast({text: null, type: null})
            }, 5000)
            
            return () => clearInterval(intervalId);
        }
    }, [toast])

    return (
        toast && <div className="position-fixed top-0 end-0 p-3" style={{ zIndex: 11 }}>
            <div id="liveToast" className={`toast show ${toastTypes[type]}`} role="alert" aria-live="assertive" aria-atomic="true">
                <div className="float-end">
                    <button type="button" className="btn-close" data-bs-dismiss="toast" aria-label="Close" onClick={() => setToast(null)}></button>
                </div>
                <div className="toast-body mx-3">
                    {toast}
                </div>
            </div>
        </div> 
    )
}

export default Toast