const Switcher = ({value, onChange}) => {
    return (
        <div className="d-flex justify-content-center">
            <span className="mx-2">Test</span>
                <div className="form-switch">
                    <input className="form-check-input" type="checkbox" onChange={({target}) => onChange(target.checked)} value={value} />
                </div>
            <span>Official</span>
        </div>
    )
}

export default Switcher