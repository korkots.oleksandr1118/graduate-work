import moment from "moment";
import { useState, useEffect } from "react"
import { getAllResults, deleteResult } from "../api/resultService.js"


const ResultTablePage = () => {
    const [results, setResults] = useState([])

    useEffect(() => {
        getAllResults().then(({data}) => {
            setResults(data) 
        })
    }, [])  

    const handleDelete = (id) => {
        deleteResult(id).then(({data}) => {setResults(results.filter(result => result.Id !== data.Id))})
    }

    return (
        <div>
            <table className="table align-middle">
                <thead>
                    <tr className="text-center">
                        <td>Name</td>
                        <td>Algorithm type</td>
                        <td>Mark</td>
                        <td>Started at</td>
                        <td>Total time</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    {results.map((result) => (
                        <tr key={`result_${result.UserId}_${result.AlgorithmTypeId}`} className="text-center">
                            <td>{`${result.User.FirstName} ${result.User.MiddleName}`}</td>
                            <td>{result.AlgorithmTypeName}</td>
                            <td>{result.Mark}</td>
                            <td>{result.StartedAt}</td>
                            <td>{moment.duration(result.ExecutionTime, "milliseconds").format()}</td>
                            <td className="d-flex justify-content-center">
                                <button className="btn btn-danger mx-1" onClick={() => handleDelete(result.Id)}>Delete</button>
                            </td>
                        </tr>
                    ))}
                </tbody>    
            </table>
        </div>
    )
}

export default ResultTablePage