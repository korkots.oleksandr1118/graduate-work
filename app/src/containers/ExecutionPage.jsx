import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import SortArray from '../components/SortArray'
import MergeSortArray from '../components/MergeSortArray'
import { 
    getCurrentUserExecutionStatus, 
    sendExecutionStatus, 
    timeExpired
} from '../api/executionService'
import Timer from "../components/Timer";


const ExecutionPage = ({managePage}) => {
    const navigate = useNavigate();
    const { executionStatus, setExecutionStatus } = managePage
    const [swapElement, setSwapElement] = useState("_")

    useEffect(() => {
        if (executionStatus.AlgorithmName === "") {
            getCurrentUserExecutionStatus(managePage) 
                .then(({navigate_to}) => navigate_to && navigate(navigate_to, { replace: true }))
        }
    }, [])
    
    const onChangeArray = (newArray) => {
        setExecutionStatus({...executionStatus, CurrentArray: newArray})
    }
    
    const nextExecutionStep = () => {
        sendExecutionStatus(managePage)
            .then(({navigate_to}) => navigate_to && navigate(navigate_to, { replace: true }))
    }

    const onTimerStop = () => {
        timeExpired(managePage)
            .then(({navigate_to}) => navigate_to && navigate(navigate_to, { replace: true }))
    }

    return (
        <div className="border border-dark p-2">
            <div className="h4 text-center">{executionStatus.AlgorithmName}</div>
            <div className="d-flex justify-content-around my-2">
                {executionStatus.RemainTime && <Timer remainTime={executionStatus.RemainTime} timeExpired={onTimerStop} />}
                <div>Failed Steps: {executionStatus.FailedStepAmount}</div>
            </div>
            {
                executionStatus.CurrentArray ?     
                (
                    executionStatus.AlgorithmName !== "MergeSort" ?
                    <SortArray currentArray={executionStatus.CurrentArray} 
                        onChange={onChangeArray}
                        swapElement={swapElement}
                        setSwapElement={setSwapElement}
                        managePage={managePage}
                        nextExecutionStep={nextExecutionStep}/>
                    : <MergeSortArray firstStepArray={executionStatus.FirstStepArray}
                        currentArray={executionStatus.CurrentArray} 
                        onChange={onChangeArray}
                        stepsCount={executionStatus.StepsCount}
                        managePage={managePage}
                        currentStepNumber={executionStatus.CurrentStepNumber}
                        nextExecutionStep={nextExecutionStep}/>       
                )
                : null
            }
        </div>
    )
}

export default ExecutionPage
