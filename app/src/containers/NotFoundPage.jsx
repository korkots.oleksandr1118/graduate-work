const NotFoundPage = () => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="text-center">
                        <h1 className="my-4">
                            Oops!</h1>
                        <h2>
                            404 Not Found</h2>
                        <div className="error-details">
                            Sorry, an error has occured, Requested page not found!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NotFoundPage;