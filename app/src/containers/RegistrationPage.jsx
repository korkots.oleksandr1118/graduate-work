import UserForm from '../components/UserForm.jsx'
import { useState } from 'react'
import { createUser } from '../api/userService.js'
import { useNavigate } from "react-router-dom";
 
const profileInfoDefault = {
    firstName: '',
    middleName: '',
    lastName: '',
    login: '',
    password: '',
}

const fields = [
    {
        label: 'First name',
        name: 'firstName',
    },
    {
        label: 'Middle name',
        name: 'middleName'
    },
    {
        label: 'Last name',
        name: 'lastName'
    },
    {
        label: 'Login', 
        name: 'login',           
    },
    {
        label: 'Password',
        name: 'password',
        type: 'password'
    },
]

const RegistrationPage = () => {    
    const [profileInfo, setProfileInfo] = useState(profileInfoDefault)
    const navigate = useNavigate();

    const onSave = () => {        
        createUser(profileInfo)
        .then(() => navigate("/login", { replace: true }))
    }

    return (
        <UserForm fields={fields} 
                    values={profileInfo} 
                    onChange={setProfileInfo}
                    onSave={onSave}
        />
    )
}

export default RegistrationPage