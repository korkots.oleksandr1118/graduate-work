import UserForm from '../components/UserForm.jsx'
import { useState } from 'react'
import { useNavigate } from "react-router-dom";
import { loginUser } from '../api/userService.js';
import { getCurrentUser } from '../api/userService'
 
const LoginInfoDefault = {
    login: '',
    password: '',
}

const LoginPage = ({managePage}) => {    
    const [loginInfo, setLoginInfo] = useState(LoginInfoDefault)
    const navigate = useNavigate();

    const onSave = () => {        
        loginUser(loginInfo, managePage)
            .then(({navigate_to}) => {
                if (navigate_to) {
                    getCurrentUser(managePage.setUser)
                    navigate(navigate_to, { replace: true })
                }
            })
    }

    const fields = [
        {
            label: 'Login', 
            name: 'login',           
        },
        {
            label: 'Password',
            name: 'password',
            type: 'password'
        },
    ]

    return (
        <UserForm fields={fields} 
                  values={loginInfo} 
                  onChange={setLoginInfo}
                  onSave={onSave}
        />
    )
}

export default LoginPage