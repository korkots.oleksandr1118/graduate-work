import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom";
import { getAllAlgorithmTypes } from '../api/algerithmTypeService'
import { createExecition } from '../api/executionService'
import Switcher from "../components/Switcher";


const SelectAlgorithmTypePage = ({managePage}) => {
    const navigate = useNavigate();
    const { algorithmTypes } = managePage
    const [isOfficial, setIsOfficial] = useState(false)

    useEffect(() => {
        if (algorithmTypes.length === 0) {
            getAllAlgorithmTypes(managePage)
                .then(({navigate_to}) => navigate_to && navigate(navigate_to, { replace: true }))
        }
    }, [])

    const onSelectAlgorithmType = (type) => {
        createExecition(type, !isOfficial, managePage)
            .then(({navigate_to}) => navigate_to && navigate(navigate_to, { replace: true }))
    }

    return (
        <>
            <div>
                <div className="text-center mb-2">
                    <span>Select execution type</span>
                </div>
                <Switcher value={isOfficial} onChange={setIsOfficial}/>
            </div>
            <div className="list-group mt-2">
                {algorithmTypes.map((type) => 
                    <button className="list-group-item list-group-item-action list-group-item-info my-1" 
                            key={`type_${type.Id}`}
                            onClick={() => onSelectAlgorithmType(type)}>
                        {type.Name}
                    </button>)
                }
            </div>
        </>
    )
}

export default SelectAlgorithmTypePage