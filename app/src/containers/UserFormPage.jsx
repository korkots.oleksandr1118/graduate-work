import { useState, useEffect } from "react"
import { useParams, useNavigate } from "react-router-dom"
import { createUser, getUser, editUser } from '../api/userService'
import UserForm from '../components/UserForm'

const UserFormPage = ({managePage}) => {
    const [userInfo, setUserInfo] = useState({})
    const navigate = useNavigate();
    let { userId } = useParams()
    
    useEffect(() => {
        if (userId != null) {
            getUser(userId, managePage)
                .then(({data, navigate_to}) => {
                    setUserInfo(data)
                    return navigate_to && navigate(navigate_to, { replace: true })
                })
                .catch(({navigate_to}) => {
                    return navigate_to && navigate(navigate_to, { replace: true })})
        }
    }, [])

    const onSave = () => {   
        if (userId != null) {
            editUser(userInfo, managePage)
                .then(() => navigate("/users", { replace: true }))
            return
        }     
        createUser(userInfo, managePage)
            .then(() => navigate("/users", { replace: true }))
    }
    

    const fields = [
        { label: 'First name', name: 'FirstName' },
        { label: 'Middle name', name: 'MiddleName' },
        { label: 'Last name', name: 'Lastname' },
        { label: 'Role', name: 'RoleId' },
        { label: 'Login',  name: 'Login' },
        { label: 'Can do official execution',  name: 'CanDoOfficialExecution' },
        { label: `Password ${userId ? "(set for change)": ""}`, name: 'Password', type: 'password' },
    ]

    return (
        <UserForm fields={fields} 
                values={userInfo} 
                onChange={setUserInfo}
                onSave={onSave}
        />
    )
}

export default UserFormPage
