import { useState, useEffect } from "react"
import { useNavigate } from "react-router-dom";
import { getAllUsers, deleteUser } from "../api/userService"

const roles = {
    1: "admin",
    2: "teacher",
    3: "student"
}

const UsersTablePage = ({managePage}) => {
    const [users, setUsers] = useState([])
    const navigate = useNavigate();

    useEffect(() => {
        getAllUsers(managePage)
            .then(({data, navigate_to}) => {
                setUsers(data)
                navigate_to && navigate(navigate_to, { replace: true })
            })
            .catch(({navigate_to}) => navigate_to && navigate(navigate_to, { replace: true }))
    }, [])  
    
    const handleDelete = (id) => {
        deleteUser(id).then(({data}) => setUsers(users.filter(user => user.Id === data.Id)))
            .catch(({navigate_to}) => navigate_to && navigate(navigate_to, { replace: true }))
    }

    return (
        users && <div>
            <button className="btn btn-primary" onClick={() => navigate("/users/create", { replace: true })}>Create new user</button>
            <table className="table align-middle text-center">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Login</td>
                        <td>First Name</td>
                        <td>Middle Name</td>
                        <td>Last Name</td>
                        <td>Can do official execution</td>
                        <td>Role</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    {users.map((user) => (
                        <tr key={`user_${user.Id}`}>
                            <td>{user.Id}</td>
                            <td>{user.Login}</td>
                            <td>{user.FirstName}</td>
                            <td>{user.MiddleName}</td>
                            <td>{user.LastName}</td>
                            <td>{user.CanDoOfficialExecution.toString()}</td>
                            <td>{roles[user.RoleId]}</td>
                            <td className="d-flex justify-content-center">
                                <button className="btn btn-info" onClick={() => navigate(`/users/${user.Id}/edit`, { replace: true })}>Edit</button>
                                <button className="btn btn-danger mx-1" onClick={() => handleDelete(user.Id)}>Delete</button>
                            </td>
                        </tr>
                    ))}
                </tbody>    
            </table>
        </div>
    )
}

export default UsersTablePage