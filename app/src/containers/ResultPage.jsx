import { useEffect } from "react";
import moment from 'moment'
import momentDurationFormatSetup  from 'moment-duration-format'
import { useNavigate } from "react-router-dom";

momentDurationFormatSetup(moment)

const colourMarkRanges = {
    "success": {min: 81, max: 100},
    "warning": {min: 51, max: 80},
    "danger": {min: 0, max: 50},
}

const ResultPage = ({managePage}) => {
    const {executionResult} = managePage
    const navigate = useNavigate()

    useEffect(() => {
        if (!executionResult.AlgorithmTypeName) {
            goToSelectAlgorithm()
        }
    }, [executionResult])
    
    const getColourByMark = () => {
        return Object.keys(colourMarkRanges).find((key) => (
            colourMarkRanges[key].min <= executionResult.Mark && executionResult.Mark <= colourMarkRanges[key].max 
        ))
    }

    const goToSelectAlgorithm = () => {
        navigate("/", { replace: true })
    }

    const fields = {
        "Algorithm:": executionResult.AlgorithmTypeName,
        "Failed steps:": executionResult.FailedStep,
        "Execution:": executionResult.IsTestExecution ? "test" : "official",
        "Execution time:": moment.duration(executionResult.ExecutionTime, "milliseconds").format()
    }

    return (
        <div className="mx-auto" style={{width: 400}}>
            <div className="border border-dark p-2 px-4 ">
                <div className={"py-4 mb-2 text-center bg-" + getColourByMark()}>
                    <span className="fs-1">{executionResult.Mark}</span>
                </div>
                {Object.keys(fields).map((key) => (
                    <div className="mb-2 text-center border-bottom" key={key}>
                        <span className="me-3">{key}</span>
                        <span>{fields[key]}</span>
                    </div>
                ))}
            </div>
            <div className="mt-2 d-flex justify-content-end" >
                <button className="btn btn-primary" onClick={goToSelectAlgorithm}>Select Algorithm</button>
            </div>
        </div>
    )
}

export default ResultPage
