import api from "./api"
import ApiEndPoints from "./ApiEndPoints"


const getAllResults = () => {
    return api.get(ApiEndPoints.GET_ALL_RESULTS)
}

const deleteResult = (id) => {
    return api.delete(ApiEndPoints.GET_RESULT(id))
}

export {
    getAllResults,
    deleteResult,
}