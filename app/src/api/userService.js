import api from "./api"
import ApiEndPoints from "./ApiEndPoints"

const checkAuthResponce = (responce, managePage) => {
    if (responce.status === 200) {
        managePage.setExecutionStatus(responce.data)
        return { navigate_to: "/execution" }
    } else if (responce.status === 202) {
        managePage.setAlgorithmTypes(responce.data)
        return { navigate_to: "/" }
    }
}

const checkUserResponce = (responce) => {
    if (responce.status === 200) {
        return { navigate_to: null, data: responce.data }
    } 
}

const handleErrorResponce = (response, managePage) => {
    if (response.status === 403) {
        managePage.setShowToast({text: "You haven`t access", type: "warning"})
        return { navigate_to: "/", data: null }
    } 
}

const loginUser = (loginInfo, managePage) => {
    return api.post(ApiEndPoints.LOGIN_USER, loginInfo)
        .then((responce) => checkAuthResponce(responce, managePage))
}
const logoutUser = (setUser) => {
    return api.get(ApiEndPoints.LOGOUT_USER)
        .then(() => {
            setUser({})
            return {navigate_to: "/login"}
        })
}
const createUser = (userInfo) => {
    userInfo.CanDoOfficialExecution = userInfo.CanDoOfficialExecution === "true"
    return api.post(ApiEndPoints.CREATE_USER, userInfo)
}
const getAllUsers = (managePage) => {
    return api.get(ApiEndPoints.GET_ALL_USERS)
        .then(checkUserResponce)
        .catch((error) => handleErrorResponce(error.response, managePage))
}

const editUser = (userInfo, managePage) => {
    userInfo.CanDoOfficialExecution = userInfo.CanDoOfficialExecution === "true"
    return api.patch(ApiEndPoints.GET_USER(userInfo.Id), userInfo)
        .then(checkUserResponce)
        .catch((error) => handleErrorResponce(error.response, managePage))
}

const getCurrentUser = (setUser) => {
    return api.get(ApiEndPoints.GET_CURRENT_USER)
        .then(({data}) => setUser(data))
}

const getUser = (id, managePage) => {
    return api.get(ApiEndPoints.GET_USER(id))
        .then(checkUserResponce)
        .catch((error) => handleErrorResponce(error.response, managePage))
}

const deleteUser = (id, managePage) => {
    return api.delete(ApiEndPoints.GET_USER(id))
        .then(checkUserResponce)
        .catch((error) => handleErrorResponce(error.response, managePage))
}

export {
    createUser,
    getAllUsers,
    loginUser,
    logoutUser,
    editUser,
    getUser,
    deleteUser,
    getCurrentUser
}
