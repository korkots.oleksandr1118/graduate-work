import api from "./api"
import ApiEndPoints from "./ApiEndPoints"

const checkResponce = (responce, managePage) => {
    if (responce.status === 202) {
        managePage.setExecutionStatus(responce.data)
        return { navigate_to: "/execution" }
    } else if (responce.status === 200) {
        managePage.setAlgorithmTypes(responce.data)
        return { navigate_to: null }
    }
} 

const getAllAlgorithmTypes = (managePage) => {
    return api.get(ApiEndPoints.GET_ALL_ALGORITHM_TYPES)
        .then((responce) => checkResponce(responce, managePage))
}

export {
    getAllAlgorithmTypes
}