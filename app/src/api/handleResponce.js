const StatusesActions = {
    401: () => {
        window.history.pushState(null, "", `/login`)
        document.location.reload()
    }
}

const handleResponce = (status) => {
    const statusAction = StatusesActions[status]
    if (statusAction) {
        statusAction()
        return true
    }
    return status >= 200 && status < 300
}

export default handleResponce
     