const ApiEndPoints = {
    CREATE_USER: '/Auth/register',
    GET_ALL_USERS: '/User',
    GET_CURRENT_USER: '/User/current',
    GET_USER: (id) => `/User/${id}`,
    GET_ALL_RESULTS: "/ExecutionResult",
    GET_RESULT: (id) => `/ExecutionResult/${id}`,
    LOGOUT_USER: '/Auth/logout',
    LOGIN_USER: '/Auth/login',
    GET_ALL_ALGORITHM_TYPES: '/AlgorithmTypes/get_all_types',
    CREATE_EXECUTION: '/Execution/create',
    GET_CURRENT_USER_EXECUTION_STATUS: '/Execution/current_user_execution_status',
    SEND_EXECUTION_STATUS: '/Execution/send_execution',
    EXECUTION_EXPIRED: '/Execution/time_expired',

}

export default ApiEndPoints