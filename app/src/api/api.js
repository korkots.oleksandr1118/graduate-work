import axios from "axios";
import handleResponce from "./handleResponce";

const domain = 'http://localhost:5000'

export default axios.create({
    baseURL: domain,
    withCredentials: true,
    validateStatus: function (status) {
        return handleResponce(status)
    }
})