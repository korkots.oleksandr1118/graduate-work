import api from "./api"
import ApiEndPoints from "./ApiEndPoints"

const defaultExecutionStatus = {
    AlgorithmName: "",
    IsTestExecution: null,
    CurrentStepNumber: 0,
    FailedStepAmount: 0,
    CurrentArray: [],
    RemainTime: null,
  }

const checkResponce = (responce, managePage, onSuccess) => {
    if (responce.status === 200) {
        onSuccess && onSuccess()
        managePage.setExecutionStatus(responce.data)
        return { navigate_to: null }
    } else if (responce.status === 202) {
        managePage.setExecutionStatus(defaultExecutionStatus)
        managePage.setAlgorithmTypes(responce.data)
        return { navigate_to: "/" }
    } else if (responce.status === 201) {
        managePage.setExecutionStatus(defaultExecutionStatus)
        managePage.setExecutionResult(responce.data)
        return { navigate_to: "/execution_result" }
    }
} 
const handleErrorResponce = (responce, managePage) => {
    if (responce.status === 409) {
        managePage.setShowToast({text: "Step is incorrect. Try again", type: "danger"})
        managePage.setExecutionStatus(responce.data)
        return { navigate_to: null }
    } else if (responce.status === 400) {
        managePage.setExecutionStatus(defaultExecutionStatus)
        managePage.setExecutionResult(responce.data)
        return { navigate_to: "/execution_result" }
    }
}

const createExecition = (selectedAlgorithmType, isTest, managePage) => {
    return api.post(ApiEndPoints.CREATE_EXECUTION, {selectedAlgorithmType, isTest})
        .then(({data}) => {
            managePage.setExecutionStatus(data)
            return { navigate_to: "/execution" }
        })
        .catch(({response}) => {
            managePage.setShowToast({text: response?.data, type: "danger"})
            return { navigate_to: null }
        })
}

const sendExecutionStatus = (managePage) => {
    const onSuccess = () => managePage.setShowToast({text: "Step is correct. Keep it up", type: "success"})

    return api.post(ApiEndPoints.SEND_EXECUTION_STATUS, managePage.executionStatus)
        .then((responce) => checkResponce(responce, managePage, onSuccess))
        .catch((error) => handleErrorResponce(error.response, managePage))
}

const getCurrentUserExecutionStatus = (managePage) => {
    return api.get(ApiEndPoints.GET_CURRENT_USER_EXECUTION_STATUS)
        .then((responce) => checkResponce(responce, managePage))
}

const timeExpired = (managePage) => {
    return api.get(ApiEndPoints.EXECUTION_EXPIRED)
        .then(({data}) => {
            managePage.setExecutionStatus(defaultExecutionStatus)
            managePage.setExecutionResult(data)
            return { navigate_to: "/execution_result" }
        })
}

export {
    createExecition,
    sendExecutionStatus,
    getCurrentUserExecutionStatus,
    timeExpired
}