using System;
using System.Text;
using api.Entities;
using api.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text.Json;
using api.DTO;

namespace api.Filters
{
    public class ExecutionResultFilter : IActionFilter
    {
        public UserRoleService UserRoleService { get; }

        public ExecutionResultFilter(UserRoleService UserRoleService)
        {
            this.UserRoleService = UserRoleService;
        }
        public void OnActionExecuting(ActionExecutingContext context)
        {
            User userFromAuthFilter = (User)context.HttpContext.Response.HttpContext.Items["currentUser"];
            if (userFromAuthFilter.RoleId != UserRoleService.admin.Id && userFromAuthFilter.RoleId != UserRoleService.teacher.Id) {
                context.Result = new ContentResult {
                    StatusCode = StatusCodes.Status403Forbidden
                };
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}