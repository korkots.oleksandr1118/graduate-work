using System;
using System.Text;
using api.Entities;
using api.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text.Json;
using api.DTO;

namespace api.Filters
{
    public class AuthFilter : IActionFilter
    {
        private readonly AlgorithmTypeService algorithmTypeService;
        private readonly ExecutionStatusService executionStatusService;

        public AuthFilter(AlgorithmTypeService algorithmTypeService, ExecutionStatusService executionStatusService)
        {
            this.algorithmTypeService = algorithmTypeService;
            this.executionStatusService = executionStatusService;
        }
        public void OnActionExecuting(ActionExecutingContext context)
        {
            // Do something after the action executes.
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.HttpContext.Response.StatusCode != 200)
            {
                return;
            }
            
            User userFromAuthFilter = (User)context.HttpContext.Response.HttpContext.Items["currentUser"];
            if (userFromAuthFilter.ExecutionStatusId == null)
            {
                context.Result = new ContentResult {
                    StatusCode = new AcceptedResult().StatusCode,
                    Content = JsonSerializer.Serialize(algorithmTypeService.GetAllAlgorismTypes())
                };
            }
            else 
            {
                ExecutionStatus userStatus = executionStatusService.GetExecutionStatusById((int)userFromAuthFilter.ExecutionStatusId);
                context.Result = new ContentResult {   
                    StatusCode = new OkResult().StatusCode,
                    Content = JsonSerializer.Serialize( new ExecutionDTO() {
                        UserFM = userFromAuthFilter.FirstName.ToString() + userFromAuthFilter.MiddleName.ToString(),
                        StepsCount = userStatus.CorrectSortSteps.StepsCount(),
                        AlgorithmName = userStatus.AlgorithmType.Name,
                        IsTestExecution = userStatus.IsTestExecution,
                        CurrentStepNumber = userStatus.CurrentStep,
                        FailedStepAmount = userStatus.FailedStepAmount,
                        CurrentArray = userStatus.CorrectSortSteps.getArrayByStep(userStatus.CurrentStep),
                        FirstStepArray = userStatus.CorrectSortSteps.GetFirstStepArray(),
                        RemainTime = (userStatus.ExpiredAt - DateTime.Now).TotalMilliseconds,
                    })
                };
            }
        }

        private TimeSpan GetRemainTime(ExecutionStatus userStatus)
        {
            if (userStatus.ExpiredAt > userStatus.StartedAt)
            {
                return userStatus.ExpiredAt - userStatus.StartedAt;
            }
            return new TimeSpan();
        }
    }
}