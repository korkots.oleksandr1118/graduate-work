using System;
using System.Text;
using api.Entities;
using api.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace api.Filters
{
    public class AuthActionFilter : IActionFilter
    {
        private readonly JwtService jwtService;
        private readonly UserService userService;

        public AuthActionFilter(JwtService jwtService, UserService userService)
        {
            this.jwtService = jwtService;
            this.userService = userService;
        }
        public void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                var securityTocken = jwtService.Verify(context.HttpContext.Request.Cookies["jwt"]);
                int currentUserId = Convert.ToInt32(securityTocken.Payload.Iss);
                User currentUser = userService.GetUsersById(currentUserId);
                context.HttpContext.Items["currentUser"] = currentUser;
            }
            catch (System.Exception)
            {
                context.Result = new UnauthorizedResult();
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            // Do something after the action executes.
        }
    }
}