using System;
using System.Text;
using api.Entities;
using api.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text.Json;
using api.DTO;

namespace api.Filters
{
    public class ExecutionStatusFilter : IActionFilter
    {
        private readonly AlgorithmTypeService algorithmTypeService;
        private readonly ExecutionStatusService executionStatusService;

        public ExecutionStatusFilter(AlgorithmTypeService algorithmTypeService, ExecutionStatusService executionStatusService)
        {
            this.algorithmTypeService = algorithmTypeService;
            this.executionStatusService = executionStatusService;
        }
        public void OnActionExecuting(ActionExecutingContext context)
        {
            User userFromAuthFilter = (User)context.HttpContext.Response.HttpContext.Items["currentUser"];
            if (userFromAuthFilter.ExecutionStatusId == null)
            {
                context.Result = new ContentResult {
                    StatusCode = new AcceptedResult().StatusCode,
                    Content = JsonSerializer.Serialize(algorithmTypeService.GetAllAlgorismTypes())
                };
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {

        }
    }
}