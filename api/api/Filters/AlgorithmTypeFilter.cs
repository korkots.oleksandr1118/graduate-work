using System;
using System.Text;
using api.Entities;
using api.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text.Json;
using api.DTO;

namespace api.Filters
{
    public class AlgorithmTypeFilter : IActionFilter
    {
        private readonly AlgorithmTypeService algorithmTypeService;
        private readonly ExecutionStatusService executionStatusService;

        public AlgorithmTypeFilter(AlgorithmTypeService algorithmTypeService, ExecutionStatusService executionStatusService)
        {
            this.algorithmTypeService = algorithmTypeService;
            this.executionStatusService = executionStatusService;
        }
        public void OnActionExecuting(ActionExecutingContext context)
        {
            User userFromAuthFilter = (User)context.HttpContext.Response.HttpContext.Items["currentUser"];
            if (userFromAuthFilter.ExecutionStatusId != null)
            {
                ExecutionStatus userStatus = executionStatusService.GetExecutionStatusById((int)userFromAuthFilter.ExecutionStatusId);
                context.Result = new ContentResult {   
                    StatusCode = new AcceptedResult().StatusCode,
                    Content = JsonSerializer.Serialize( new ExecutionDTO() {
                        UserFM = userFromAuthFilter.FirstName.ToString() + userFromAuthFilter.MiddleName.ToString(),
                        StepsCount = userStatus.CorrectSortSteps.StepsCount(),
                        AlgorithmName = userStatus.AlgorithmType.Name,
                        IsTestExecution = userStatus.IsTestExecution,
                        CurrentStepNumber = userStatus.CurrentStep,
                        FailedStepAmount = userStatus.FailedStepAmount,
                        FirstStepArray = userStatus.CorrectSortSteps.GetFirstStepArray(),
                        CurrentArray = userStatus.CorrectSortSteps.getArrayByStep(userStatus.CurrentStep),
                        RemainTime = (userStatus.ExpiredAt - DateTime.Now).TotalMilliseconds,
                    })
                };
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {   
            
        }

        private TimeSpan GetRemainTime(ExecutionStatus userStatus)
        {
            if (userStatus.ExpiredAt > userStatus.StartedAt)
            {
                return userStatus.ExpiredAt - userStatus.StartedAt;
            }
            return new TimeSpan();
        }
    }
}