﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class AddColumnCanDoOfficialExecutionToUsersTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "can_do_official_execution",
                table: "users",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "can_do_official_execution", "password" },
                values: new object[] { true, "$2a$11$B2yBPURwM8ZAAQh.SKcHXO67oSDJoW5TGEwuTY5mXan3qPCby.kK2" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "can_do_official_execution",
                table: "users");

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                column: "password",
                value: "$2a$11$lhRxZF4K941znmb4hw2cwOTiSt7WCTYrghpE1Ny7go6/EiWArHivu");
        }
    }
}
