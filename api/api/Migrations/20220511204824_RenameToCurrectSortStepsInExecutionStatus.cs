﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class RenameToCurrectSortStepsInExecutionStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "sorting_steps",
                table: "execution_statuses",
                newName: "correct_sort_steps");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "correct_sort_steps",
                table: "execution_statuses",
                newName: "sorting_steps");
        }
    }
}
