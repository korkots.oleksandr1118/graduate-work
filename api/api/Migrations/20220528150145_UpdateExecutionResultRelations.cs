﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class UpdateExecutionResultRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                column: "password",
                value: "$2a$11$AIH2Oe.HaCoLkxwyLN6iNurlnmjRcybm3lyiSHl8vGf3wAGDL5a12");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                column: "password",
                value: "$2a$11$1DkdvAfBfapCAhAvTWmK4uSUldm4xfMfBRR8bI9vJImDEREPZ1Zjq");
        }
    }
}
