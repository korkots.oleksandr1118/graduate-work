﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class ChangeKeyPrioratyForResult : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "pk_execution_results",
                table: "execution_results");

            migrationBuilder.DropIndex(
                name: "ix_execution_results_user_id",
                table: "execution_results");

            migrationBuilder.AddPrimaryKey(
                name: "pk_execution_results",
                table: "execution_results",
                columns: new[] { "user_id", "algorithm_type_id" });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                column: "password",
                value: "$2a$11$qK1bUf1zCWpXrwI8NmkUcebrnLyLmCs91WaAVhFn4Un7aprd.H8Zi");

            migrationBuilder.CreateIndex(
                name: "ix_execution_results_algorithm_type_id",
                table: "execution_results",
                column: "algorithm_type_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "pk_execution_results",
                table: "execution_results");

            migrationBuilder.DropIndex(
                name: "ix_execution_results_algorithm_type_id",
                table: "execution_results");

            migrationBuilder.AddPrimaryKey(
                name: "pk_execution_results",
                table: "execution_results",
                columns: new[] { "algorithm_type_id", "user_id" });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                column: "password",
                value: "$2a$11$AIH2Oe.HaCoLkxwyLN6iNurlnmjRcybm3lyiSHl8vGf3wAGDL5a12");

            migrationBuilder.CreateIndex(
                name: "ix_execution_results_user_id",
                table: "execution_results",
                column: "user_id");
        }
    }
}
