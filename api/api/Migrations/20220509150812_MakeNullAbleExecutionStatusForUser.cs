﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class MakeNullAbleExecutionStatusForUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_users_execution_statuses_execution_status_id",
                table: "users");

            migrationBuilder.AlterColumn<int>(
                name: "execution_status_id",
                table: "users",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "fk_users_execution_statuses_execution_status_id",
                table: "users",
                column: "execution_status_id",
                principalTable: "execution_statuses",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_users_execution_statuses_execution_status_id",
                table: "users");

            migrationBuilder.AlterColumn<int>(
                name: "execution_status_id",
                table: "users",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "fk_users_execution_statuses_execution_status_id",
                table: "users",
                column: "execution_status_id",
                principalTable: "execution_statuses",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
