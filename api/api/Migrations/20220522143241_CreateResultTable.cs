﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class CreateResultTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "execution_results",
                columns: table => new
                {
                    user_id = table.Column<int>(type: "integer", nullable: false),
                    algorithm_type_id = table.Column<int>(type: "integer", nullable: false),
                    execution_status_id = table.Column<int>(type: "integer", nullable: false),
                    mark = table.Column<int>(type: "integer", nullable: false),
                    execution_time = table.Column<TimeSpan>(type: "interval", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_execution_results", x => new { x.algorithm_type_id, x.user_id });
                    table.ForeignKey(
                        name: "fk_execution_results_algorithm_types_algorithm_type_id",
                        column: x => x.algorithm_type_id,
                        principalTable: "algorithm_types",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_execution_results_execution_statuses_execution_status_id",
                        column: x => x.execution_status_id,
                        principalTable: "execution_statuses",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_execution_results_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_execution_results_execution_status_id",
                table: "execution_results",
                column: "execution_status_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_execution_results_user_id",
                table: "execution_results",
                column: "user_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "execution_results");
        }
    }
}
