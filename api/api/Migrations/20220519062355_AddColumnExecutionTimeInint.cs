﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class AddColumnExecutionTimeInint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "execution_time",
                table: "algorithm_types",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 1,
                column: "execution_time",
                value: 20);

            migrationBuilder.UpdateData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 2,
                column: "execution_time",
                value: 20);

            migrationBuilder.UpdateData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 3,
                column: "execution_time",
                value: 20);

            migrationBuilder.UpdateData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 4,
                column: "execution_time",
                value: 20);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "execution_time",
                table: "algorithm_types");
        }
    }
}
