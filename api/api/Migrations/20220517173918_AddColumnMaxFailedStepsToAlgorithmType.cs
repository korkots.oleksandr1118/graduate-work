﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class AddColumnMaxFailedStepsToAlgorithmType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "max_failed_steps",
                table: "algorithm_types",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 1,
                column: "max_failed_steps",
                value: 3);

            migrationBuilder.UpdateData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 2,
                column: "max_failed_steps",
                value: 3);

            migrationBuilder.UpdateData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 3,
                column: "max_failed_steps",
                value: 3);

            migrationBuilder.UpdateData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 4,
                column: "max_failed_steps",
                value: 3);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "max_failed_steps",
                table: "algorithm_types");
        }
    }
}
