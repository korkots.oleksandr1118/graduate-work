﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class AddExecutionTimeToExecutionType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<TimeSpan>(
                name: "execution_time",
                table: "algorithm_types",
                type: "interval",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.UpdateData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 1,
                column: "execution_time",
                value: new TimeSpan(0, 0, 20, 0, 0));

            migrationBuilder.UpdateData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 2,
                column: "execution_time",
                value: new TimeSpan(0, 0, 20, 0, 0));

            migrationBuilder.UpdateData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 3,
                column: "execution_time",
                value: new TimeSpan(0, 0, 20, 0, 0));

            migrationBuilder.UpdateData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 4,
                column: "execution_time",
                value: new TimeSpan(0, 0, 20, 0, 0));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "execution_time",
                table: "algorithm_types");
        }
    }
}
