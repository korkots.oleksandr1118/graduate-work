﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using api.Entities;

namespace api.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20220511204824_RenameToCurrectSortStepsInExecutionStatus")]
    partial class RenameToCurrectSortStepsInExecutionStatus
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.12")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            modelBuilder.Entity("api.Models.AlgorithmType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<TimeSpan>("ExecutionTime")
                        .HasColumnType("interval")
                        .HasColumnName("execution_time");

                    b.Property<string>("Name")
                        .HasColumnType("text")
                        .HasColumnName("name");

                    b.HasKey("Id")
                        .HasName("pk_algorithm_types");

                    b.ToTable("algorithm_types");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            ExecutionTime = new TimeSpan(0, 0, 20, 0, 0),
                            Name = "BubbleSort"
                        },
                        new
                        {
                            Id = 2,
                            ExecutionTime = new TimeSpan(0, 0, 20, 0, 0),
                            Name = "SelectionSort"
                        },
                        new
                        {
                            Id = 3,
                            ExecutionTime = new TimeSpan(0, 0, 20, 0, 0),
                            Name = "InsertionSort"
                        },
                        new
                        {
                            Id = 4,
                            ExecutionTime = new TimeSpan(0, 0, 20, 0, 0),
                            Name = "MergeSort"
                        });
                });

            modelBuilder.Entity("api.Models.ExecutionStatus", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int>("AlgorithmTypeId")
                        .HasColumnType("integer")
                        .HasColumnName("algorithm_type_id");

                    b.Property<string>("CorrectSortSteps")
                        .HasColumnType("text")
                        .HasColumnName("correct_sort_steps");

                    b.Property<int>("CurrentStep")
                        .HasColumnType("integer")
                        .HasColumnName("current_step");

                    b.Property<DateTime>("ExpiredAt")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("expired_at");

                    b.Property<int>("FailedStepAmount")
                        .HasColumnType("integer")
                        .HasColumnName("failed_step_amount");

                    b.Property<bool>("IsTestExecution")
                        .HasColumnType("boolean")
                        .HasColumnName("is_test_execution");

                    b.Property<DateTime>("StartedAt")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("started_at");

                    b.Property<DateTime>("UpdateAt")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("update_at");

                    b.HasKey("Id")
                        .HasName("pk_execution_statuses");

                    b.HasIndex("AlgorithmTypeId")
                        .HasDatabaseName("ix_execution_statuses_algorithm_type_id");

                    b.ToTable("execution_statuses");
                });

            modelBuilder.Entity("api.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int?>("ExecutionStatusId")
                        .HasColumnType("integer")
                        .HasColumnName("execution_status_id");

                    b.Property<string>("FirstName")
                        .HasColumnType("text")
                        .HasColumnName("first_name");

                    b.Property<string>("Lastname")
                        .HasColumnType("text")
                        .HasColumnName("lastname");

                    b.Property<string>("Login")
                        .HasColumnType("text")
                        .HasColumnName("login");

                    b.Property<string>("MiddleName")
                        .HasColumnType("text")
                        .HasColumnName("middle_name");

                    b.Property<string>("Password")
                        .HasColumnType("text")
                        .HasColumnName("password");

                    b.Property<int>("RoleId")
                        .HasColumnType("integer")
                        .HasColumnName("role_id");

                    b.HasKey("Id")
                        .HasName("pk_users");

                    b.HasIndex("ExecutionStatusId")
                        .HasDatabaseName("ix_users_execution_status_id");

                    b.HasIndex("RoleId")
                        .HasDatabaseName("ix_users_role_id");

                    b.ToTable("users");
                });

            modelBuilder.Entity("api.Models.UserRole", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Name")
                        .HasColumnType("text")
                        .HasColumnName("name");

                    b.HasKey("Id")
                        .HasName("pk_user_roles");

                    b.ToTable("user_roles");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "admin"
                        },
                        new
                        {
                            Id = 2,
                            Name = "teacher"
                        },
                        new
                        {
                            Id = 3,
                            Name = "student"
                        });
                });

            modelBuilder.Entity("api.Models.ExecutionStatus", b =>
                {
                    b.HasOne("api.Models.AlgorithmType", "AlgorithmType")
                        .WithMany()
                        .HasForeignKey("AlgorithmTypeId")
                        .HasConstraintName("fk_execution_statuses_algorithm_types_algorithm_type_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("AlgorithmType");
                });

            modelBuilder.Entity("api.Models.User", b =>
                {
                    b.HasOne("api.Models.ExecutionStatus", "ExecutionStatus")
                        .WithMany()
                        .HasForeignKey("ExecutionStatusId")
                        .HasConstraintName("fk_users_execution_statuses_execution_status_id");

                    b.HasOne("api.Models.UserRole", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .HasConstraintName("fk_users_user_roles_role_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ExecutionStatus");

                    b.Navigation("Role");
                });
#pragma warning restore 612, 618
        }
    }
}
