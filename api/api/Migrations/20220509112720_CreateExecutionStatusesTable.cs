﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace api.Migrations
{
    public partial class CreateExecutionStatusesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "execution_status_id",
                table: "users",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "execution_statuses",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    algorithm_type_id = table.Column<int>(type: "integer", nullable: false),
                    is_test_execution = table.Column<bool>(type: "boolean", nullable: false),
                    failed_step_amount = table.Column<int>(type: "integer", nullable: false),
                    sorting_steps = table.Column<string>(type: "text", nullable: true),
                    current_step = table.Column<int>(type: "integer", nullable: false),
                    started_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    expired_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_execution_statuses", x => x.id);
                    table.ForeignKey(
                        name: "fk_execution_statuses_algorithm_types_algorithm_type_id",
                        column: x => x.algorithm_type_id,
                        principalTable: "algorithm_types",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_users_execution_status_id",
                table: "users",
                column: "execution_status_id");

            migrationBuilder.CreateIndex(
                name: "ix_execution_statuses_algorithm_type_id",
                table: "execution_statuses",
                column: "algorithm_type_id");

            migrationBuilder.AddForeignKey(
                name: "fk_users_execution_statuses_execution_status_id",
                table: "users",
                column: "execution_status_id",
                principalTable: "execution_statuses",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_users_execution_statuses_execution_status_id",
                table: "users");

            migrationBuilder.DropTable(
                name: "execution_statuses");

            migrationBuilder.DropIndex(
                name: "ix_users_execution_status_id",
                table: "users");

            migrationBuilder.DropColumn(
                name: "execution_status_id",
                table: "users");
        }
    }
}
