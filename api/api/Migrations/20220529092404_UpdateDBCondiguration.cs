﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class UpdateDBCondiguration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                column: "password",
                value: "$2a$11$odiX1eW4/0s/h/Adl6Bya.1ME3r0tTRJ83Kv39fJp8Ju4aPfxb7VC");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                column: "password",
                value: "$2a$11$qK1bUf1zCWpXrwI8NmkUcebrnLyLmCs91WaAVhFn4Un7aprd.H8Zi");
        }
    }
}
