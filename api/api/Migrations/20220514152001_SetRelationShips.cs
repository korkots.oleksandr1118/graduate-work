﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class SetRelationShips : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "ix_users_execution_status_id",
                table: "users");

            migrationBuilder.CreateIndex(
                name: "ix_users_execution_status_id",
                table: "users",
                column: "execution_status_id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "ix_users_execution_status_id",
                table: "users");

            migrationBuilder.CreateIndex(
                name: "ix_users_execution_status_id",
                table: "users",
                column: "execution_status_id");
        }
    }
}
