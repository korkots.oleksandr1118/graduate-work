﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class CreateAlgorithmTypeSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "algorithm_types",
                columns: new[] { "id", "name" },
                values: new object[,]
                {
                    { 1, "BubbleSort" },
                    { 2, "SelectionSort" },
                    { 3, "InsertionSort" },
                    { 4, "MergeSort" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "algorithm_types",
                keyColumn: "id",
                keyValue: 4);
        }
    }
}
