﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace api.Migrations
{
    public partial class AddIDToResult : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "pk_execution_results",
                table: "execution_results");

            migrationBuilder.AddColumn<int>(
                name: "id",
                table: "execution_results",
                type: "integer",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddPrimaryKey(
                name: "pk_execution_results",
                table: "execution_results",
                column: "id");

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                column: "password",
                value: "$2a$11$lhRxZF4K941znmb4hw2cwOTiSt7WCTYrghpE1Ny7go6/EiWArHivu");

            migrationBuilder.CreateIndex(
                name: "ix_execution_results_user_id",
                table: "execution_results",
                column: "user_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "pk_execution_results",
                table: "execution_results");

            migrationBuilder.DropIndex(
                name: "ix_execution_results_user_id",
                table: "execution_results");

            migrationBuilder.DropColumn(
                name: "id",
                table: "execution_results");

            migrationBuilder.AddPrimaryKey(
                name: "pk_execution_results",
                table: "execution_results",
                columns: new[] { "user_id", "algorithm_type_id" });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                column: "password",
                value: "$2a$11$odiX1eW4/0s/h/Adl6Bya.1ME3r0tTRJ83Kv39fJp8Ju4aPfxb7VC");
        }
    }
}
