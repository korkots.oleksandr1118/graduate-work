using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Models;
using api.Entities;
using api.Service;
using api.Filters;
using api.DTO;

namespace api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [ServiceFilter(typeof(AuthActionFilter))]
    [ServiceFilter(typeof(ExecutionResultFilter))]
    public class ExecutionResultController : ControllerBase
    {
        private readonly ExecutionResultService executionResultService;

        public ExecutionResultController(ExecutionResultService executionResultService)
        {
            this.executionResultService = executionResultService;
        }

        [HttpGet("")]
        public ActionResult<List<ExecutionResultDTO>> GetAllResults()
        {   
            return Ok(executionResultService.GetAllResults());
        }

        [HttpGet("{id}")]
        public ActionResult<ExecutionResultDTO> GetResult(int id)
        {   
            return Ok(executionResultService.GetResultsById(id));
        }

        [HttpDelete("{id}")]
        public ActionResult<ExecutionResult> DeleteResultById(int id)
        {
            return Ok(executionResultService.DeleteResult(id));
        }
    }
}