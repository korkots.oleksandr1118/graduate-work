using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Entities;
using api.Service;
using api.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using api.Filters;

namespace api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly UserService userService;
        private readonly JwtService jwtService;
        public AuthController(UserService userService, JwtService jwtService)
        {
            this.userService = userService;
            this.jwtService = jwtService;
        }
        
        [HttpPost("register")]
        public ActionResult Register(RegistrationDTO model)
        {
            if (!userService.IsContainLogin(model.Login))
            {
                userService.CreateUserByRegistration(model);
                return Ok();   
            }
            else 
            {
                return BadRequest();
            }
        }

        [HttpPost("login")]
        [ServiceFilter(typeof(AuthFilter))]
        public ActionResult Login(LoginDTO model)
        {            
            if (userService.IsContainLogin(model.Login))
            {   
                try
                {
                    User currentUser = userService.IsCorrectCredential(model);
                    string jwt = jwtService.Generate(currentUser.Id);                                       
                    Response.Cookies.Append("jwt", jwt);
                    HttpContext.Items["currentUser"] = currentUser;
                    return Ok();   
                }
                catch (System.ArgumentException)
                {
                    HttpContext.Response.StatusCode = new BadRequestResult().StatusCode;
                    Response.Cookies.Delete("jwt");
                }
            }
            return BadRequest();            
        }

        [HttpGet("logout")]
        public ActionResult Logout()
        {
            Response.Cookies.Delete("jwt");

            return Ok();
        }
    }
}