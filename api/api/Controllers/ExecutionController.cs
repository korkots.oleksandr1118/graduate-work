using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Entities;
using api.Filters;
using api.Service;
using api.DTO;

namespace api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [ServiceFilter(typeof(AuthActionFilter))]
    public class ExecutionController : ControllerBase
    {
        private readonly ExecutionStatusService executionStatusService;
        private readonly ExecutionResultService executionResultService;

        public ExecutionController(ExecutionStatusService executionStatusService, ExecutionResultService executionResultService)
        {
            this.executionStatusService = executionStatusService;
            this.executionResultService = executionResultService;
        }

        [HttpPost("create")]
        public ActionResult CreateExecution(CreateExecutionDTO createExecutionDTO)
        {
            try
            {
                return Ok(executionStatusService.CreateExecutionStatus(createExecutionDTO));
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("current_user_execution_status")]
        [ServiceFilter(typeof(ExecutionStatusFilter))]
        public ActionResult GetCurrentUserExecutionStatus()
        {
            return Ok(executionStatusService.GetCurrentUserExecutionDTO());
        }

        [HttpGet("time_expired")]
        [ServiceFilter(typeof(ExecutionStatusFilter))]
        public ActionResult<ExecutionResultDTO> GetTimeExpired()
        {
            return Ok(executionStatusService.CreateResult());
        }

        [HttpPost("send_execution")]
        [ServiceFilter(typeof(ExecutionStatusFilter))]
        public ActionResult SendExecution(ExecutionDTO execution)
        {
            try
            {
                return Ok(executionStatusService.CheckAnswer(execution));
            }
            catch (IndexOutOfRangeException)
            {
                return Created("/execution_result", executionStatusService.CreateResult());
            }
            catch (ArgumentException)
            {
                return Conflict(execution);
            }
        }
    }
}