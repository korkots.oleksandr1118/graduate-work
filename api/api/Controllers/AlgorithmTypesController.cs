using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Entities;
using api.Service;
using api.Filters;

namespace api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [ServiceFilter(typeof(AuthActionFilter))]
    [ServiceFilter(typeof(AlgorithmTypeFilter))]
    public class AlgorithmTypesController : ControllerBase
    {
        private AlgorithmTypeService algorithmTypeService;

        public AlgorithmTypesController(AlgorithmTypeService algorithmTypeService )
        {
            this.algorithmTypeService = algorithmTypeService;
        }

        [HttpGet("get_all_types")]
        public ActionResult<IEnumerable<AlgorithmType>> GetAlgorithTypes()
        {
            return Ok(algorithmTypeService.GetAllAlgorismTypes());
        }
    }
}