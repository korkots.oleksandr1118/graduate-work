using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Models;
using api.Entities;
using api.Service;
using api.Filters;

namespace api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [ServiceFilter(typeof(AuthActionFilter))]
    public class UserController : ControllerBase
    {
        private readonly UserService userService;

        public UserController(UserService userService)
        {
            this.userService = userService;
        }

        [HttpGet("")]
        [ServiceFilter(typeof(UserFilter))]
        public ActionResult<IEnumerable<User>> GetUsers()
        {   
            return Ok(userService.GetAllUsers());
        }
        
        [HttpGet("current")]
        public ActionResult<IEnumerable<User>> GetCurrentUser()
        {   
            return Ok(userService.GetCurrentUser());
        }

        [HttpGet("{id}")]
        [ServiceFilter(typeof(UserFilter))]
        public ActionResult<User> GetUser(int id)
        {   
            return Ok(userService.GetUsersById(id));
        }

        [HttpPost("")]
        [ServiceFilter(typeof(UserFilter))]
        public ActionResult<User> PostUser(User model)
        {
            return Ok(userService.CreateUser(model));
        }

        [HttpPatch("{id}")]
        [ServiceFilter(typeof(UserFilter))]
        public IActionResult PutUser(int id, User model)
        {
            return Ok(userService.UpdateUser(model));
        }

        [HttpDelete("{id}")]
        [ServiceFilter(typeof(UserFilter))]
        public ActionResult<User> DeleteUserById(int id)
        {
            return Ok(userService.DeleteUser(id));
        }
    }
}