using System;
using api.Entities;

namespace api.DTO
{
    public class ExecutionResultDTO
    {
        public int Id { get; set; }
        public User User { get; set; }
        public int Mark { get; set; }
        public string StartedAt { get; set; }
        public double ExecutionTime { get; set; }
        public string AlgorithmTypeName { get; set; }
        public int FailedStep { get; set; }
        public bool IsTestExecution { get; set; }
    }
}