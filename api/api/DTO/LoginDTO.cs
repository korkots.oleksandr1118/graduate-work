using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace api.DTO
{
    public class LoginDTO
    {        
        public string Login { get; set; }
        public string Password { get; set; }
    }
}