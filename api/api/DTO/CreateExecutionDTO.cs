using System;
using api.Entities;

namespace api.DTO
{
    public class CreateExecutionDTO
    {
        public AlgorithmType SelectedAlgorithmType { get; set; }
        public bool IsTest { get; set; }
    }
}