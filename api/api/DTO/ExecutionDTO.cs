using System;

namespace api.DTO
{
    public class ExecutionDTO
    {
        public string UserFM { get; set; }
        public string AlgorithmName { get; set; }
        public int StepsCount { get; set; }
        public bool IsTestExecution { get; set; }
        public int[] FirstStepArray { get; set; }
        public int[] CurrentArray { get; set; }
        public int FailedStepAmount { get; set; }
        public int CurrentStepNumber { get; set; }
        public double RemainTime { get; set; }
    }
}