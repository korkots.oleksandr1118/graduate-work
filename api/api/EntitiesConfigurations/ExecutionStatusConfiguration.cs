using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using api.Models;

namespace api.Entities
{
    public class ExecutionStatusEntityTypeConfiguration : IEntityTypeConfiguration<ExecutionStatus>
    {
        public void Configure(EntityTypeBuilder<ExecutionStatus> builder)
        {
            builder
                .Property(b => b.CorrectSortSteps)
                .HasConversion(
                    v => v.ToString(),
                    v => new SortSteps(v)
                );
        }
    }
}