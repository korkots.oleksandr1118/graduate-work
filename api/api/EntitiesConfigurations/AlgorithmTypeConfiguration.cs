using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace api.Entities
{
    public class AlgorithmTypeEntityTypeConfiguration : IEntityTypeConfiguration<AlgorithmType>
    {
        Dictionary<string, int> typeDefaultExecutionTime = new Dictionary<string, int> {
            {AlgorithmTypes.BubbleSort.ToString(), 20},
            {AlgorithmTypes.SelectionSort.ToString(), 20},
            {AlgorithmTypes.InsertionSort.ToString(), 20},
            {AlgorithmTypes.MergeSort.ToString(), 20},
        };
        Dictionary<string, int> typeDefaultMaxFailedSteps = new Dictionary<string, int> {
            {AlgorithmTypes.BubbleSort.ToString(), 3},
            {AlgorithmTypes.SelectionSort.ToString(), 3},
            {AlgorithmTypes.InsertionSort.ToString(), 3},
            {AlgorithmTypes.MergeSort.ToString(), 3},
        };

        public void Configure(EntityTypeBuilder<AlgorithmType> builder)
        {
            builder
                .HasData(GetSeedDataFromTypeEnum());
        }
        public AlgorithmType[] GetSeedDataFromTypeEnum()
        {
            List<AlgorithmType> seedData = new List<AlgorithmType>();

            foreach (var typeByEnum in Enum.GetValues(typeof(AlgorithmTypes)))
            {
                AlgorithmType newType = new AlgorithmType {
                    Id = (int)typeByEnum, 
                    Name = typeByEnum.ToString()
                };

                if (typeDefaultExecutionTime.ContainsKey(typeByEnum.ToString()))
                {
                    newType.ExecutionTime = typeDefaultExecutionTime[typeByEnum.ToString()];
                }
                if (typeDefaultMaxFailedSteps.ContainsKey(typeByEnum.ToString()))
                {
                    newType.MaxFailedSteps = typeDefaultMaxFailedSteps[typeByEnum.ToString()];
                }

                seedData.Add(newType);
            }

            return seedData.ToArray();
        }
    }
}