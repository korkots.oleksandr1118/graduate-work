using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Text.Json;

namespace api.Entities
{
    public class UserRoleEntityTypeConfiguration : IEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder
                .HasData(
                    new UserRole {Id = (int)Roles.admin, Name = Roles.admin.ToString()},
                    new UserRole {Id = (int)Roles.teacher, Name = Roles.teacher.ToString()},
                    new UserRole {Id = (int)Roles.student, Name = Roles.student.ToString()}
                );
        }
    }
}