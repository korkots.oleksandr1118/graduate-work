using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using api.Models;
using api.Service;

namespace api.Entities
{
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .Property(p => p.CanDoOfficialExecution)
                .HasDefaultValue(false);

            builder
                .HasData(
                    new User {
                        Id = 1,
                        Login = "admin",
                        FirstName = "Admin",
                        MiddleName = "Admin",
                        Password = BCrypt.Net.BCrypt.HashPassword("123456"),
                        RoleId = (int)Roles.admin,
                        CanDoOfficialExecution = true
                    }
                );
        }
    }
}