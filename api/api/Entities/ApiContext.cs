using Microsoft.EntityFrameworkCore;
using api.Entities;

namespace api.Entities 
{
    public class ApiContext : DbContext 
    {
        public ApiContext() { }
        public ApiContext(DbContextOptions<ApiContext> options) : base (options) { }   
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<AlgorithmType> AlgorithmTypes { get; set; }
        public DbSet<ExecutionStatus> ExecutionStatuses { get; set; }
        public DbSet<ExecutionResult> ExecutionResults { get; set; }     
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            new ExecutionStatusEntityTypeConfiguration().Configure(modelBuilder.Entity<ExecutionStatus>());
            new UserRoleEntityTypeConfiguration().Configure(modelBuilder.Entity<UserRole>());
            new AlgorithmTypeEntityTypeConfiguration().Configure(modelBuilder.Entity<AlgorithmType>());
            new UserEntityTypeConfiguration().Configure(modelBuilder.Entity<User>());
        } 
    }
}