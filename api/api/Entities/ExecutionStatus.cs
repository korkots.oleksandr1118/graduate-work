using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using api.Models;

namespace api.Entities
{
    public class ExecutionStatus
    {
        public int Id { get; set; }
        public int AlgorithmTypeId { get; set; }
        public AlgorithmType AlgorithmType { get; set; }
        public bool IsTestExecution { get; set; }
        public int FailedStepAmount { get; set; }
        public SortSteps CorrectSortSteps { get; set; }
        public int CurrentStep { get; set; }
        public DateTime StartedAt { get; set; }
        public DateTime UpdateAt { get; set; }
        public DateTime ExpiredAt { get; set; }
        public User User { get; set; }
        public ExecutionResult ExecutionResult { get; set; }
    }
}