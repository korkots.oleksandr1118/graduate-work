using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace api.Entities
{
    public class User
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string MiddleName { get; set; }
        public string Lastname { get; set; }
        public string Login { get; set; }
        public bool CanDoOfficialExecution { get; set; }
        [JsonIgnore]
        public string Password { get; set; }
        public int RoleId { get; set; }
        [JsonIgnore]
        public UserRole Role { get; set; }
        public int? ExecutionStatusId { get; set; }
        [JsonIgnore]
        public ExecutionStatus ExecutionStatus { get; set; }
        [JsonIgnore]
        public List<ExecutionResult> ExecutionResults { get; set; }
    }
}