using System;
using System.Text.Json.Serialization;

namespace api.Entities
{
    public class ExecutionResult
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int AlgorithmTypeId { get; set; }
        public AlgorithmType AlgorithmType { get; set; }
        public int ExecutionStatusId { get; set; }
        public ExecutionStatus ExecutionStatus { get; set; }
        public int Mark { get; set; }
        public TimeSpan ExecutionTime { get; set; }
    }
}