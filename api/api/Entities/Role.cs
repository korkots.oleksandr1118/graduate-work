using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace api.Entities
{
    public class UserRole
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public List<User> Users { get; set; } 
    }

    public enum Roles
    {
        admin = 1,
        teacher = 2,
        student = 3
    }
}