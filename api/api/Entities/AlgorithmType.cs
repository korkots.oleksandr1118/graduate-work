using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace api.Entities
{
    public class AlgorithmType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ExecutionTime { get; set; }
        public int MaxFailedSteps { get; internal set; }
        public List<ExecutionStatus> ExecutionStatuses { get; set; }
        public List<ExecutionResult> ExecutionResults { get; set; }
    }
    public enum AlgorithmTypes
    {
        BubbleSort = 1,
        SelectionSort = 2,
        InsertionSort = 3,
        MergeSort = 4,
    }
}