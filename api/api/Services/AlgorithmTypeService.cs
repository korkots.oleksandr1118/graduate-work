using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api.Entities;
using api.DTO;

namespace api.Service
{
    public class AlgorithmTypeService
    {
        ApiContext db;
        public readonly AlgorithmType bubbleSort;
        public readonly AlgorithmType selectionSort;
        public readonly AlgorithmType insertionSort;
        public readonly AlgorithmType mergeSort;
        public AlgorithmTypeService(ApiContext db)
        {
            this.db = db;
            this.bubbleSort = db.AlgorithmTypes.Find((int)AlgorithmTypes.BubbleSort);
            this.selectionSort = db.AlgorithmTypes.Find((int)AlgorithmTypes.SelectionSort);
            this.insertionSort = db.AlgorithmTypes.Find((int)AlgorithmTypes.InsertionSort);
            this.mergeSort = db.AlgorithmTypes.Find((int)AlgorithmTypes.MergeSort);
        }
        public List<AlgorithmType> GetAllAlgorismTypes()
        {
            return db.AlgorithmTypes.ToList();
        }
        public AlgorithmType GetAlgorithmTypeById(int id)
        {
            return db.AlgorithmTypes.Find(id);
        }
    }
}