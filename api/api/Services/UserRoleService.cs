using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api.Entities;
using api.DTO;

namespace api.Service
{
    public class UserRoleService
    {
        ApiContext db;
        public readonly UserRole admin;
        public readonly UserRole teacher;
        public readonly UserRole student;
        public UserRoleService(ApiContext db)
        {
            this.db = db;
            this.admin = db.UserRoles.Find((int)Roles.admin);
            this.teacher = db.UserRoles.Find((int)Roles.teacher);
            this.student = db.UserRoles.Find((int)Roles.student);
        }

        public UserRole GetRoleById(int id)
        {
            return db.UserRoles.Find(id);
        }
    }
}