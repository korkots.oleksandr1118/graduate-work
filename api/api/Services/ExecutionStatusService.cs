using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api.Entities;
using api.Models;
using api.Models.SortAlgorithms;
using Microsoft.AspNetCore.Http;
using api.DTO;

namespace api.Service
{
    public class ExecutionStatusService
    {
        ApiContext db;
        UserService userService;
        private AlgorithmTypeService algorithmTypeService;
        private IHttpContextAccessor httpContextAccessor;
        private ExecutionResultService executionResultService;

        public ExecutionStatusService(ApiContext db, UserService userService, AlgorithmTypeService algorithmTypeService, IHttpContextAccessor httpContextAccessor, ExecutionResultService executionResultService )
        {
            this.db = db;
            this.userService = userService;
            this.algorithmTypeService = algorithmTypeService;
            this.httpContextAccessor = httpContextAccessor;
            this.executionResultService = executionResultService;
        }
        public ExecutionStatus GetExecutionStatusById(int id)
        {
            ExecutionStatus status = db.ExecutionStatuses.Find(id);
            if (status == null)
            {
                throw new Exception("ExecutionStatus not found");
            }
            return status;
        }

        internal void DeleteCurrentUserExecution()
        {
            ExecutionStatus currentUserExecution =  GetCurrentUserExecutionStatus();
            userService.DeleteCurrentUserExecutionReference();
            db.ExecutionStatuses.Remove(currentUserExecution);
            db.SaveChanges();
        }

        public ExecutionDTO CreateExecutionStatus(CreateExecutionDTO createExecution)
        {
            AlgorithmType selectedAlgorismType = createExecution.SelectedAlgorithmType;
            bool IsTestExecution = createExecution.IsTest;
            if (!IsTestExecution) {
                executionResultService.CheckPresentExecutionInResults(selectedAlgorismType);
            }

            SortAlgorithm currentSortAlgorithm = new SortAlgorithm(RandomArray.GenerateArray(), selectedAlgorismType);
            ExecutionStatus userExecutionStatus = new ExecutionStatus() {
                AlgorithmTypeId = selectedAlgorismType.Id, 
                AlgorithmType = algorithmTypeService.GetAlgorithmTypeById(selectedAlgorismType.Id),
                IsTestExecution = IsTestExecution,
                FailedStepAmount = 0,
                CorrectSortSteps = currentSortAlgorithm.GetSortSteps(),
                CurrentStep = 0,
                StartedAt = DateTime.Now,
                UpdateAt = DateTime.Now,
                ExpiredAt = DateTime.Now.AddMinutes(selectedAlgorismType.ExecutionTime),
            };
            db.ExecutionStatuses.Add(userExecutionStatus);
            userService.AddUserExecutionStatus(userExecutionStatus);
            db.SaveChanges();
            return GetCurrentUserExecutionDTO();
        }

        internal ExecutionDTO GetCurrentUserExecutionDTO()
        {
            ExecutionStatus currentUserExecution = GetCurrentUserExecutionStatus();
            return new ExecutionDTO() {
                UserFM = currentUserExecution.User.FirstName.ToString() + " " + currentUserExecution.User.MiddleName.ToString(),
                StepsCount = currentUserExecution.CorrectSortSteps.StepsCount(),
                AlgorithmName = currentUserExecution.AlgorithmType.Name,
                IsTestExecution = currentUserExecution.IsTestExecution,
                CurrentStepNumber = currentUserExecution.CurrentStep,
                FailedStepAmount = currentUserExecution.FailedStepAmount,
                CurrentArray = currentUserExecution.CorrectSortSteps.getArrayByStep(currentUserExecution.CurrentStep),
                FirstStepArray = currentUserExecution.CorrectSortSteps.GetFirstStepArray(),
                RemainTime = (currentUserExecution.ExpiredAt - DateTime.Now).TotalMilliseconds,
            };
        }

        internal ExecutionStatus GetCurrentUserExecutionStatus()
        {
            User currentUser = (User)httpContextAccessor.HttpContext.Items["currentUser"];
            ExecutionStatus currentUserExecution = GetExecutionStatusById((int)currentUser.ExecutionStatusId);
            currentUserExecution.User = currentUser;
            return currentUserExecution;
        }
        internal ExecutionResultDTO CreateResult()
        {
            ExecutionStatus currentUserExecution = GetCurrentUserExecutionStatus();
            int executionMark = CalcMark(currentUserExecution);
            TimeSpan executionTime = currentUserExecution.UpdateAt - currentUserExecution.StartedAt;

            ExecutionResultDTO executionResultDTO = new ExecutionResultDTO {
                User = currentUserExecution.User, 
                AlgorithmTypeName = currentUserExecution.AlgorithmType.Name,
                FailedStep = currentUserExecution.FailedStepAmount,
                IsTestExecution = currentUserExecution.IsTestExecution,
                StartedAt = currentUserExecution.StartedAt.ToString(),
                Mark = executionMark,
                ExecutionTime = executionTime.TotalMilliseconds,
            };

            if (currentUserExecution.IsTestExecution) {
                DeleteCurrentUserExecution();
            } else {
                executionResultService.SaveExecutionResult(currentUserExecution, executionMark);
            }
            
            return executionResultDTO;
        }

        private string CheckString(string str)
        {
            if (str == null) {
                return "";
            }
            return str;
        }

        private int CalcMark(ExecutionStatus currentUserExecution)
        {
            int step = currentUserExecution.CurrentStep;
            int failedSteps = currentUserExecution.FailedStepAmount;

            double oneStepMark = 100 / currentUserExecution.CorrectSortSteps.StepsCount();
            double oneFAiledStepMark = oneStepMark / 10;

            double mark = (step + 1) * oneStepMark - failedSteps * oneFAiledStepMark;
            if (mark < 0)
            {
                return 0;
            }
            return Convert.ToInt32(mark);
        }

        internal ExecutionDTO CheckAnswer(ExecutionDTO execution)
        {
            ExecutionStatus currentUserExecution = GetCurrentUserExecutionStatus();
            currentUserExecution.UpdateAt = DateTime.Now;
            if (currentUserExecution.ExpiredAt < DateTime.Now || 
                execution.FailedStepAmount >= currentUserExecution.AlgorithmType.MaxFailedSteps)
            {
                throw new IndexOutOfRangeException();
            }
            if (!currentUserExecution.CorrectSortSteps.CompareStep(execution.CurrentStepNumber + 1, execution.CurrentArray)) 
            {
                execution.FailedStepAmount++;
                currentUserExecution.FailedStepAmount++;
                execution.CurrentArray = currentUserExecution.CorrectSortSteps.getArrayByStep(execution.CurrentStepNumber);
                db.ExecutionStatuses.Update(currentUserExecution);
                db.SaveChanges();
                throw new ArgumentException();
            }
            execution.CurrentStepNumber++;
            currentUserExecution.CurrentStep++;
            db.SaveChanges();
            return execution;
        }
    }
}