using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api.Entities;
using api.DTO;
using Microsoft.AspNetCore.Http;

namespace api.Service
{
    public class UserService
    {
        ApiContext db;
        private UserRoleService userRoleService;
        private IHttpContextAccessor httpContextAccessor;

        public UserService(ApiContext db, UserRoleService userRoleService, IHttpContextAccessor httpContextAccessor)
        {
            this.db = db;
            this.userRoleService = userRoleService;
            this.httpContextAccessor = httpContextAccessor;
            
        }
        internal User CreateUserByRegistration(RegistrationDTO model)
        {
            User newUser = new User() {                
                FirstName = model.FirstName,
                MiddleName = model.MiddleName,
                Lastname = model.Lastname,
                Login = model.Login,
                ExecutionStatus = null,
                ExecutionStatusId = null,
                RoleId = userRoleService.student.Id,
                Role = userRoleService.student,
                Password = BCrypt.Net.BCrypt.HashPassword(model.Password)
            };
            db.Users.Add(newUser);
            db.SaveChanges();

            return newUser;
        }

        internal User DeleteUser(int id)
        {
            User user = GetUsersById(id);
            db.Users.Remove(user);
            db.SaveChanges();
            
            return user;
        }

        internal User UpdateUser(User user)
        {
            User editingUser = GetUsersById(user.Id);

            editingUser.FirstName = user.FirstName; 
            editingUser.MiddleName = user.MiddleName;
            editingUser.Lastname = user.Lastname;
            editingUser.RoleId = user.RoleId;
            editingUser.Role = userRoleService.GetRoleById(user.RoleId);
            editingUser.Login = user.Login;
            editingUser.CanDoOfficialExecution = user.CanDoOfficialExecution;
            
            if (user.Password != null) {
                editingUser.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);
            }
            
            db.Users.Update(editingUser);
            db.SaveChanges();
            return editingUser;
        }

        internal User CreateUser(User newUser)
        {
            newUser.Password = BCrypt.Net.BCrypt.HashPassword(newUser.Password);
            db.Users.Add(newUser);
            db.SaveChanges();
            return newUser;
        }

        internal User GetCurrentUser()
        {
            int currentUserId = ((User)httpContextAccessor.HttpContext.Items["currentUser"]).Id;
            User currentUser = GetUsersById(currentUserId);
            return currentUser;
        }

        internal void DeleteCurrentUserExecutionReference()
        {
            User currentUser = GetCurrentUser();
            currentUser.ExecutionStatusId = null;
            currentUser.ExecutionStatus = null;
            db.Users.Update(currentUser);
            db.SaveChanges();
        }

        internal User GetUsersById(int currentUserId)
        {
            User user = db.Users.Find(currentUserId);
            if (user == null)
            {
                throw new Exception("user not found");                
            }
            return user;
        }

        internal IEnumerable<User> GetAllUsers()
        {
            List<User> sortedUsers = db.Users.ToList();
            sortedUsers.Sort((prev, next) => prev.Id > next.Id ? 1 : 0);
            return sortedUsers;
        }

        internal void AddUserExecutionStatus(ExecutionStatus executionStatus)
        {
            User currentUser = GetCurrentUser() ;
            currentUser.ExecutionStatusId = executionStatus.Id;
            currentUser.ExecutionStatus = executionStatus;
            db.Users.Update(currentUser);
        }

        internal bool IsContainLogin(string login)
        {
            return db.Users.FirstOrDefault(user => user.Login == login) != null;
        }

        internal User IsCorrectCredential(LoginDTO credential)
        {
            User user = db.Users.FirstOrDefault(user => user.Login == credential.Login);
            
            if (BCrypt.Net.BCrypt.Verify(credential.Password, user.Password))
            {   
                return user;
            }
            else 
            {
                throw new ArgumentException();
            }
        }
    }
}