using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using api.Entities;
using api.Models;
using api.Models.SortAlgorithms;
using Microsoft.AspNetCore.Http;
using api.DTO;

namespace api.Service
{
    public class ExecutionResultService
    {
        ApiContext db;
        IHttpContextAccessor httpContextAccessor;
        private UserService userService;

        public ExecutionResultService(ApiContext db, UserService userService, IHttpContextAccessor httpContextAccessor)
        {
            this.db = db;
            this.httpContextAccessor = httpContextAccessor; 
            this.userService = userService;
        }
        internal void SaveExecutionResult(ExecutionStatus currentUserExecution, int mark)
        {
            TimeSpan executionTime = currentUserExecution.UpdateAt - currentUserExecution.StartedAt;
            ExecutionResult executionResult = new ExecutionResult {
                UserId = currentUserExecution.User.Id,
                User = currentUserExecution.User,
                AlgorithmTypeId = currentUserExecution.AlgorithmType.Id,
                AlgorithmType = currentUserExecution.AlgorithmType,
                ExecutionStatusId = currentUserExecution.Id,
                ExecutionStatus = currentUserExecution,
                Mark = mark,
                ExecutionTime = executionTime,
            };
            db.ExecutionResults.Add(executionResult);
            db.SaveChanges();
            userService.DeleteCurrentUserExecutionReference();
        }

        internal ExecutionResult DeleteResult(int id)
        {
            ExecutionResult executionResult = db.ExecutionResults.Include(r => r.ExecutionStatus).Where(r => r.Id == id).Single();
            db.ExecutionStatuses.Remove(executionResult.ExecutionStatus);
            db.ExecutionResults.Remove(executionResult);
            db.SaveChanges();
            return executionResult;
        }

        internal ExecutionResultDTO GetResultsById(int id)
        {
            ExecutionResult executionResult = db.ExecutionResults.Include(r => r.ExecutionStatus).Where(res => res.Id == id).First();
            
            return new ExecutionResultDTO {
                User = executionResult.User, 
                AlgorithmTypeName = executionResult.AlgorithmType.Name,
                FailedStep = executionResult.ExecutionStatus.FailedStepAmount,
                StartedAt = executionResult.ExecutionStatus.StartedAt.ToString(),
                Mark = executionResult.Mark,
                ExecutionTime = executionResult.ExecutionTime.TotalMilliseconds,
            };
        }
        internal List<ExecutionResultDTO> GetAllResults()
        {
            List<ExecutionResultDTO> results = new List<ExecutionResultDTO>();
            List<ExecutionResult> executionResults = db.ExecutionResults
                .Include(r => r.AlgorithmType)
                .Include(r => r.ExecutionStatus)
                .Include(r => r.User)
                .ToList();

            foreach (ExecutionResult result in db.ExecutionResults)
            {
                results.Add(new ExecutionResultDTO {
                    Id = result.Id,
                    User = result.User, 
                    AlgorithmTypeName = result.AlgorithmType.Name,
                    StartedAt = result.ExecutionStatus.StartedAt.ToString(),
                    Mark = result.Mark,
                    ExecutionTime = result.ExecutionTime.TotalMilliseconds,
                });
            }
            return results;
        }
        private string CheckString(string str)
        {
            if (str == null) {
                return "";
            }
            return str;
        }

        internal void CheckPresentExecutionInResults(AlgorithmType selectedAlgorithmType)
        {
            User currentUser = (User)httpContextAccessor.HttpContext.Items["currentUser"];

            if (!currentUser.CanDoOfficialExecution) {
                throw new Exception("You haven`t permission for official execution");
            }
            if (db.ExecutionResults.FirstOrDefault(r => r.UserId == currentUser.Id && r.AlgorithmTypeId == selectedAlgorithmType.Id) != null) {
                throw new Exception("Your official result already present for this algorithm type. Сontact your teacher about this!");
            }
        }
    }
}