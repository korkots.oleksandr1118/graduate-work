using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json;

namespace api.Models
{
    public class SortSteps
    {
        List<int[]> stepsArray;

        public SortSteps(string stepsArrayInJson)
        {
            this.stepsArray = JsonSerializer.Deserialize<List<int[]>>(stepsArrayInJson, null);
        }

        public SortSteps(int[] startArray)
        {
            this.stepsArray = new List<int[]>();
            this.stepsArray.Add((int[])startArray.Clone());
        }

        public SortSteps()
        {
            this.stepsArray = new List<int[]>();
        }

        public int[] GetFirstStepArray()
        {
            return stepsArray[0];
        }

        public int StepsCount()
        {
            return stepsArray.Count();
        }

        public int[] GetResult()
        {
            return stepsArray.Last();
        }

        public bool CompareStep(int stepNumber, int[] array )
        {
            if (stepNumber >= stepsArray.Count())
            {
                throw new IndexOutOfRangeException();
            }
            return stepsArray[stepNumber].SequenceEqual(array);
        }

        internal void SetStep(int[] array, int step)
        {

            stepsArray[step] = (int[])array.Clone();
        }

        public void MergeStep(int[] right, int step)
        {
            int[] left = (int[])stepsArray[step].Clone();
            int[] combined = new int[left.Length + right.Length];
            Array.Copy(left, combined, left.Length);
            Array.Copy(right, 0, combined, left.Length, right.Length);

            stepsArray[step] = combined;
        }

        public void PushStep(int[] nextStep)
        {
            stepsArray.Add((int[])nextStep.Clone());
        }

        internal void RotateSteps(int indexStart, int indexEnd)
        {
            for (int i = 0; i <= (indexEnd - indexStart) / 2; i++)
            {
                int[] temp = stepsArray[indexStart + i];
                stepsArray[indexStart + i] = stepsArray[indexEnd - i];
                stepsArray[indexEnd - i] = temp;
            }
        }

        internal int[] getArrayByStep(int currentStep)
        {
            return stepsArray[currentStep];
        }

        public int[] getStartArray()
        {
            return stepsArray[0];
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(stepsArray, null);
        }
    }
}
