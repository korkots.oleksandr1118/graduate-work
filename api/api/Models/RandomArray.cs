using System;

namespace api.Models
{
    public static class RandomArray
    {
        const int MinValue = 0;
        const int MaxValue = 20;
        const int Length = 8;
        static public int[] GenerateArray(int length = Length, int minValue = MinValue, int maxValue = MaxValue)
        {
            Random rand = new Random();
            int[] generatedArray = new int[length];

            for (int i = 0; i < length; i++)
            {
                generatedArray[i] = rand.Next(minValue, maxValue);
            }

            return generatedArray;
        }
    }
}