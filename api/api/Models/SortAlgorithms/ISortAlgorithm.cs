using api.Models;

namespace api.Models.SortAlgorithms
{
    public interface ISortAlgorithm
    {
        public SortSteps GetSortSteps();
    }
}