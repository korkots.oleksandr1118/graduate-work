namespace api.Models.SortAlgorithms
{
    public class SelectionSort : ISortAlgorithm
    {
        int[] sortArray;
        SortSteps sortArraySteps;
        public SelectionSort(int[] sortArray)
        {
            this.sortArray = sortArray;
            SaveSortSteps();
        }
        public SortSteps GetSortSteps()
        {
            return sortArraySteps;
        }

        private void SaveSortSteps()
        {
            sortArraySteps = new SortSteps(sortArray);

            for (int i = 0; i < sortArray.Length - 1; i++)
            {
                int minValueIndex = i;
                for (int j = i + 1; j < sortArray.Length; j++)
                {
                    if (sortArray[minValueIndex] > sortArray[j])
                    {
                        minValueIndex = j;
                    }
                }
                SwapElements(minValueIndex, i);
                sortArraySteps.PushStep(sortArray);
            }
        }
        private void SwapElements(int firstIndex, int secondIndex)
        {
            int temp = sortArray[firstIndex];
            sortArray[firstIndex] = sortArray[secondIndex];
            sortArray[secondIndex] = temp;
        }
    }
}