using System;
using api.Entities;

namespace api.Models.SortAlgorithms
{  
    public class SortAlgorithm : ISortAlgorithm
    {
        ISortAlgorithm algorithm;
        public SortAlgorithm(int[] sortArray, AlgorithmType algorithmType)
        {
            switch (algorithmType.Id)
            {
                case (int)AlgorithmTypes.BubbleSort:
                    algorithm = new BubbleSort(sortArray);
                    break;
                case (int)AlgorithmTypes.SelectionSort:
                    algorithm = new SelectionSort(sortArray);
                    break;
                case (int)AlgorithmTypes.InsertionSort:
                    algorithm = new InsertionSort(sortArray);
                    break;
                case (int)AlgorithmTypes.MergeSort:
                    algorithm = new MergeSort(sortArray);
                    break;
                default:
                    throw new Exception("Unknown sort type");
            }
        }
        public SortSteps GetSortSteps()
        {
            return algorithm.GetSortSteps();
        }
        public ISortAlgorithm Algorithm
        {
            get {
                return algorithm;
            }
        }
    }

}