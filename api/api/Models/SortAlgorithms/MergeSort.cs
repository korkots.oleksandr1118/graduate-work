using System;
using System.Linq;

namespace api.Models.SortAlgorithms
{
    public class MergeSort : ISortAlgorithm
    {
        int[] sortArray;
        SortSteps sortArraySteps;

        public MergeSort(int[] sortArray)
        {
            this.sortArray = sortArray;
            SaveSortSteps();
        }

        private void SaveSortSteps()
        {
            sortArraySteps = new SortSteps();
            int[] array = (int[])sortArray.Clone();
            Sort(array, 0, array.Length - 1, 0);
            sortArraySteps.RotateSteps(0, sortArraySteps.StepsCount() - 1);
            sortArraySteps.SetStep(sortArray, 0);
        }

        private void Sort(int[] arr, int l, int r, int step)
        {
            if (sortArraySteps.StepsCount() < step + 1) {
                sortArraySteps.PushStep(new int[0]);
            }
            if (l < r) {
                int m = l + (r - l) / 2;
    
                Sort(arr, l, m, step + 1);
                Sort(arr, m + 1, r, step + 1);
                Merge(arr, l, m, r);
                sortArraySteps.MergeStep(arr.Skip(l).Take(r - l + 1).ToArray(), step); 
            }
            
        }

        private void Merge(int[] arr, int l, int m, int r)
        {
            int n1 = m - l + 1;
            int n2 = r - m;
    
            int[] L = new int[n1];
            int[] R = new int[n2];
            int i, j;
    
            for (i = 0; i < n1; ++i)
                L[i] = arr[l + i];
            for (j = 0; j < n2; ++j)
                R[j] = arr[m + 1 + j];
    
            i = 0;
            j = 0;
    
            int k = l;
            while (i < n1 && j < n2) {
                if (L[i] <= R[j]) {
                    arr[k] = L[i];
                    i++;
                }
                else {
                    arr[k] = R[j];
                    j++;
                }
                k++;
            }
    
            while (i < n1) {
                arr[k] = L[i];
                i++;
                k++;
            }

            while (j < n2) {
                arr[k] = R[j];
                j++;
                k++;
            }
        }

        public SortSteps GetSortSteps()
        {
            return sortArraySteps;
        }
    }
}