using System;

namespace api.Models.SortAlgorithms
{
    public class InsertionSort : ISortAlgorithm
    {
        int[] sortArray;
        SortSteps sortArraySteps;
        public InsertionSort(int[] sortArray)
        {
            this.sortArray = sortArray;
            SaveSortSteps();
        }

        private void SaveSortSteps()
        {
            sortArraySteps = new SortSteps(sortArray);

            for (int i = 1; i < sortArray.Length; i++)
            {
                int temp = sortArray[i];
                int j = i - 1;
                for (; j >= 0; j--)
                {
                    if (temp < sortArray[j]) {
                        sortArray[j + 1] = sortArray[j];
                    } else {
                        break;
                    }
                }
                sortArray[j + 1] = temp;
                sortArraySteps.PushStep(sortArray);
            }
        }

        public SortSteps GetSortSteps()
        {
            return sortArraySteps;
        }
    }
}