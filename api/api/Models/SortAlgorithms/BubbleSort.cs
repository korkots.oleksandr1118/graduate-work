using api.Models;
using api.Entities;

namespace api.Models.SortAlgorithms
{
    public class BubbleSort : ISortAlgorithm
    {
        int[] sortArray;
        SortSteps sortArraySteps;
        public BubbleSort(int[] sortArray)
        {
            this.sortArray = sortArray;
            SaveSortSteps();
        }
        public SortSteps GetSortSteps()
        {
            return sortArraySteps;
        }

        private void SaveSortSteps()
        {
            sortArraySteps = new SortSteps(sortArray);
            bool hasSwap = true;

            for (int i = 0; i < sortArray.Length && hasSwap; i++)
            {
                for (int j = 0; j < sortArray.Length - i - 1; j++)
                {
                    if (sortArray[j] > sortArray[j + 1])
                    {
                        SwapElements(j, j + 1);
                        hasSwap = true;
                    }
                    sortArraySteps.PushStep(sortArray);
                }
            }
        }
        private void SwapElements(int firstIndex, int secondIndex)
        {
            int temp = sortArray[firstIndex];
            sortArray[firstIndex] = sortArray[secondIndex];
            sortArray[secondIndex] = temp;
        }
    }
}