using System;
using Xunit;
using api.Models.SortAlgorithms;

namespace tests.Models.SortAlgorithms
{
    public class BubbleSortTests
    {
        [Fact]
        public void CheckSteps4Elements()
        {
            int[] randomArray = new int[] {4, 3, 2, 1};
            BubbleSort sortAlgorithm = new BubbleSort(randomArray);
            Assert.Equal("[[4,3,2,1],[3,4,2,1],[3,2,4,1],[3,2,1,4],[2,3,1,4],[2,1,3,4],[1,2,3,4]]", sortAlgorithm.GetSortSteps().ToString());
        }

        [Fact]
        public void CheckSteps8Elements()
        {
            int[] randomArray = new int[] {4, 3, 2, 1};
            BubbleSort sortAlgorithm = new BubbleSort(randomArray);
            Assert.Equal("[[4,3,2,1],[3,4,2,1],[3,2,4,1],[3,2,1,4],[2,3,1,4],[2,1,3,4],[1,2,3,4]]", sortAlgorithm.GetSortSteps().ToString());
        }
    }
}