using System;
using Xunit;
using api.Models.SortAlgorithms;

namespace tests.Models.SortAlgorithms
{
    public class SelectionSortTests
    {   
        [Fact]
        public void CheckSteps4Elements()
        {
            int[] randomArray = new int[] {4, 3, 2, 1};
            SelectionSort sortAlgorithm = new SelectionSort(randomArray);
            Assert.Equal("[[4,3,2,1],[1,3,2,4],[1,2,3,4],[1,2,3,4]]", sortAlgorithm.GetSortSteps().ToString());
        }

        [Fact]
        public void CheckSteps8Elements()
        {
            int[] randomArray = new int[] {8, 8, 3, 4, 5, 5, 2, 1};
            SelectionSort sortAlgorithm = new SelectionSort(randomArray);
            Assert.Equal("[[8,8,3,4,5,5,2,1],[1,8,3,4,5,5,2,8],[1,2,3,4,5,5,8,8],[1,2,3,4,5,5,8,8],[1,2,3,4,5,5,8,8],[1,2,3,4,5,5,8,8],[1,2,3,4,5,5,8,8],[1,2,3,4,5,5,8,8]]", sortAlgorithm.GetSortSteps().ToString());
        }
    }
}