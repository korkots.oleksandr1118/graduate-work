using System;
using Xunit;
using api.Models.SortAlgorithms;
using System.Collections.Generic;
using Xunit.Abstractions;

namespace tests.Models.SortAlgorithms
{
    public class MergeSortTests
    {
        int[] randomArray;
        MergeSort sortAlgorithm;

        [Fact]
        public void CheckSteps4Elements()
        {            
            randomArray = new int[] {4, 3, 2, 1};
            sortAlgorithm = new MergeSort(randomArray);
            Assert.Equal("[[4,3,2,1],[3,4,1,2],[1,2,3,4]]", sortAlgorithm.GetSortSteps().ToString());
        }

        [Fact]
        public void CheckSteps8Elements()
        {
            randomArray = new int[] {16, 1, 6, 10, 4, 17, 5, 19};
            sortAlgorithm = new MergeSort(randomArray);
            Assert.Equal("[[16,1,6,10,4,17,5,19],[1,16,6,10,4,17,5,19],[1,6,10,16,4,5,17,19],[1,4,5,6,10,16,17,19]]", sortAlgorithm.GetSortSteps().ToString());
        }

        [Fact]
        public void CheckStepsAnother8Elements()
        {
            randomArray = new int[] {1, 16, 16, 10, 18, 4, 5, 19};
            sortAlgorithm = new MergeSort(randomArray);
            Assert.Equal("[[1,16,16,10,18,4,5,19],[1,16,10,16,4,18,5,19],[1,10,16,16,4,5,18,19],[1,4,5,10,16,16,18,19]]", sortAlgorithm.GetSortSteps().ToString());
        }
    }
}