using System;
using Xunit;
using api.Models.SortAlgorithms;
using api.Entities;

namespace tests.Models.SortAlgorithms
{
    public class SortAlgorithmTests
    {
        int[] randomArray;
        SortAlgorithm sortAlgorithm;
        
        [Fact]
        public void SortAlgorithmOfSelectionSort()
        {
            randomArray = new int[] {4, 3, 2, 1};
            
            AlgorithmType algorithmType = CreateAlgorithmType(AlgorithmTypes.SelectionSort);
            sortAlgorithm = new SortAlgorithm(randomArray, algorithmType);

            Assert.IsType<SelectionSort>(sortAlgorithm.Algorithm);
        }

        [Fact]
        public void SortAlgorithmOfMergeSort()
        {
            randomArray = new int[] {4, 3, 2, 1};
            
            AlgorithmType algorithmType = CreateAlgorithmType(AlgorithmTypes.MergeSort);
            sortAlgorithm = new SortAlgorithm(randomArray, algorithmType);

            Assert.IsType<MergeSort>(sortAlgorithm.Algorithm);
        }

        private AlgorithmType CreateAlgorithmType(AlgorithmTypes algorithmType)
        {
            return new AlgorithmType {
                Id = (int)algorithmType, 
                Name = algorithmType.ToString()
            };
        }
    }
}