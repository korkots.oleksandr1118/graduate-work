using System;
using Xunit;
using api.Models.SortAlgorithms;

namespace tests.Models.SortAlgorithms
{
    public class InsertionSortTests
    {
        int[] randomArray;
        InsertionSort sortAlgorithm;

        public InsertionSortTests()
        {
            randomArray = new int[] {4, 3, 2, 1};
            sortAlgorithm = new InsertionSort(randomArray);
        }

        [Fact]
        public void CheckSteps()
        {
            Assert.Equal("[[4,3,2,1],[3,4,2,1],[2,3,4,1],[1,2,3,4]]", sortAlgorithm.GetSortSteps().ToString());
        }
    }
}